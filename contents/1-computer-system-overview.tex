\section{Computer System Overview}

\subsection{Basic Elements}
At a top level, a computer consists of four basic  elements which are
shown in figure \ref{fig:1-computer-components}. This components and their respective internal
components will be described below.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{figures/include/1-computer-system-overview/computer-components}
    \caption{Top-level components of a computer}
    \label{fig:1-computer-components}
\end{figure}

\subsubsection{The Processor}
The Processor \textit{(or \textbf{CPU} when a system contains only one)} is responsible for controlling the
operation of the computer and for performing the data processing functions of the computer.

One of the operations of the processor is exchanging data with memory. For this purpose, it uses
a memory address register which contains the memory address for the next read or write operation and
a memory buffer register which contains the data that is either received from memory or to be written
to memory. A similar mechanic is used for communicating with I/O modules, using the I/O address register
and I/O buffer register respectively. Processors may also contain other registers such as an accumulator
for data processing.

\subsubsection{The Main Memory}
The main memory \textit{(or Random Access Memory (RAM))}
is a set of location defined by a sequential address, that can be written to or read from
using the address. The main memory can contain both instructions and data, but each memory location
can only hold either data or an instruction.

\subsubsection{The I/O Modules}
An IO module transfers data between external devices and the processor or the memory. To achieve this
purpose, it may contain internal buffers to temporarily store data until the data can be transferred
to the intended destination.

\subsubsection{The System Bus}
The System Bus is responsible facilitating communication between the components of the computer.


\subsection{Evolution of the Microprocessor}
Desktop and handheld computing is powered by the invention of the microprocessor, a processor on
a single chip. Nowadays, microprocessors are the fastest general-purpose processors and are usually
microprocessors with multiple processors called cores and the possibility to run more then
one thread per core.

Processors have become really good in general computing. However there is an increased demand for
numerical computation. This demand is satisfied by \textbf{Graphical Processing Units} \textit{(\textbf{GPUs})} that provide
\textbf{Single-Instruction Multiple Data} \textit{(\textbf{SIMD})} techniques.
These operations that can operate on arrays of data instead of on single values are now also being
integrated as Vector Units into x86 and AMD64 processor families.

Modern computers also contain more specialized chips such as Digital Signal Processors (DSPs)
for processing streaming signals such as audio or video, or specialized chips or CPU modules to support
hardware accelerated compute-heavy operations such as video encoding.

For handheld devices, the classic microprocessor is involving into a \textbf{System on a Chip} \textit{(\textbf{SoC})}.
An SoC is a single chip that contains most of the system components such as RAM, the CPU and the GPU.


\subsection{Instruction Execution}
\tipbox {
    For a visual explanation of the instruction cycle, watch this Tom Scott video:

    \href{https://youtu.be/Z5JC9Ve1sfI}{https://youtu.be/Z5JC9Ve1sfI}.
}

A program consists of a set of instructions stored in memory. The basic cycle to execute a program,
the \textbf{instruction cycle} \textit{(also known as the \textbf{fetch-execute cycle})},
consists of two stages: an instruction is fetched and then executed
before the next instruction is fetched and the cycle continues until either the processor
is turned off, a halt instruction is executed or a fatal error occurs.
This basic process is shown in figure \ref{fig:1-basic-instruction-cycle}.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{figures/include/1-computer-system-overview/basic-instruction-cycle}
    \caption{Basic CPU instruction cycle}
    \label{fig:1-basic-instruction-cycle}
\end{figure}

\subsubsection{Fetch stage}
The fetch stage consists of the following steps:

\begin{enumerate}
    \item Load an instruction from the memory location indicated by the program counter
        into the instruction register.
    \item \textbf{Unless instructed otherwise}, increment the program counter by 1.
\end{enumerate}

\subsubsection{Execute stage}
The execute stage consists of the following steps:

\begin{enumerate}
    \item Interpret the instruction that is present in the instruction register.
    \item Perform the action that is required by the instruction.
\end{enumerate}

\subsubsection{Instructions}
Instructions contain bits that instruct the processor to take a specific action. Generally, these
actions fall into one of the following categories:

\begin{itemize}
    \item \textbf{Memory}: Transfer to the processor from memory or from the memory to the processor.
    \item \textbf{I/O}: Transfer data from the processor to an external device or from an external
        device to the processor, by using an I/O module.
    \item \textbf{Data processing}: Perform data processing actions such as logic or arithmetic on data.
    \item \textbf{Control}: Alter the execution sequence of instructions. This is usually done
        by changing the next instruction to be executed, which is achieved by changing the program counter.
\end{itemize}

\subsection{The Memory Hierarchy}

Memory modules have three typical aspects: access speeds, cost per bit and storage capacity.
Generally, the following relationships exists:

\begin{itemize}
    \item Faster memory is more expensive.
    \item Memory with higher storage capacity is less expensive.
    \item Memory with higher storage capacity takes more time to access.
\end{itemize}

To keep computers efficient in both operations and costs, a hierarchy is introduced that is shown
in figure \ref{fig:1-memory-hierarchy}.

\begin{figure}[H]
    \centering
    \includegraphics[width=30em]{figures/include/1-computer-system-overview/memory-hierarchy}
    \caption{The memory hierarchy}
    \label{fig:1-memory-hierarchy}
\end{figure}

The CPU will use the inboard memory which is then supplemented using outboard storage and off-line
storage. The memory of the latter two is typically accessed using I/O modules and when data on this
memory is required, it is usually first loaded into inboard memory. As the CPU has to access more data
from the lower two memory tiers, I/O operations will take more time.

\subsubsection{Locality of reference}
Memory references tend to cluster together when a program is being executed. This causes less data to be
retrieved from lower-tiered storage when a program has been loaded into inboard memory because the
chance of the required data being present in inboard memory is high.

\subsubsection{Volatility}
Inboard memory is usually volatile. That is, when the computer is turned off, all data stored on the
memory module is lost. Outbound storage and off-line storage are usually non-volatile. That is, when
the computer is turned off, all data stored on the memory module is retained. Non-volatile external
memory is also named secondary memory or auxiliary memory. 


\subsection{Cache Memory}

Cache memory is invisible to the OS, sits between the CPU and the main memory and
interacts with other memory management hardware.

\subsubsection{Motivation}
Every instruction cycle, the CPU has to access memory at least once. This implies that the instruction cycle
time is limited by the time it takes to read and/or write to the main memory, which is also known as the
memory cycle time.

Over time, the speed of the CPU has increased faster then the speed for memory access. To counter the CPU being
limited by memory access operations, the locality of reference principle is exploited to cache relevant data from memory
before the CPU requests the data.

\subsubsection{Principles}
The principle of cache memory is to provide a memory access time approaching the memory access time of
the fastest type of memory available whilst supporting a large memory size made of less expensive components
that have a higher access time. To achieve this, a small amount of very fast memory is placed between the
processor. Whenever the processor requests a word from memory, first the cache is queried. If the cache contains
the requested word, the value is immediately returned to the processor. Should the cache not contain the word,
the entire block of memory that contains the word is copied into the cache whilst the word is returned to the processor.
This concept is shown in figure \ref{fig:1-single-cache}.


\begin{figure}[H]
    \centering
    \includegraphics[width=0.65\textwidth]{figures/include/1-computer-system-overview/single-cache}
    \caption{Single-level cache between the CPU and the RAM}
    \label{fig:1-single-cache}
    \textit{The bigger the square, the higher the memory capacity of the component (not to scale).}
\end{figure}

In practice, multiple levels of cache are used. This is shown in figure \ref{fig:1-multi-cache}. The main principle
is the same. As the level of the cache increases, so do both the access time and the size of the cache.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{figures/include/1-computer-system-overview/multi-cache}
    \caption{Multi-level cache between the CPU and the RAM}
    \label{fig:1-multi-cache}
    \textit{The bigger the square, the higher the memory capacity of the component (not to scale).}
\end{figure}

The internal data structure of a cache with size \(C\) and block size of \(K\) words is a list of \(C\) slots \textit{(or lines)}.
A line consists of a tag that refers to the block of main memory that the line originates from and a copy of the words inside
the block of main memory. This structure is shown in figure \ref{fig:1-cache-data-structure}.

The tag is required because there are more possible blocks in memory then there are slots in the cache. This is inevitable because
if all of the contents of the slow memory would fit into the fast cache memory, the slow memory should not be included in the system.

\begin{figure}[H]
    \centering
    \includegraphics{figures/include/1-computer-system-overview/cache-data-structure}
    \caption{Internal data structure of a cache}
    \label{fig:1-cache-data-structure}
\end{figure}

\subsubsection{Design}
A cache has certain design aspects that are listed below.

\paragraph*{Cache size} \mbox{} \newline
The size of the cache is already briefly discussed above.

\paragraph*{Block size} \mbox{} \newline
Due to the principle of locality, the hit ratio \textit{(part of the cached read request of the total of read requests)} initially
increases as the block size of the cache increases. However, as the block size increases beyond a certain point, the hit ratio starts
to decrease because too much values are evicted from the cache.

\paragraph*{Mapping function} \mbox{} \newline
The mapping function decides where a new block of data will be placed in the cache. The design of this function is affected
by two constraints:

\begin{itemize}
    \item When a new block is written to the cache, an old block may have to be removed. It would be beneficial if the replaced block
        is not needed in the near future.
    \item If the mapping function is more complex \textit{(or flexible)}, more complex circuitry is required to implement the
        mapping function.
\end{itemize}

\paragraph*{Replacement algorithm} \mbox{} \newline
Which is the algorithm that the mapping function delegates the actual searching of the block that is to be replaced to.
It is impossible to correctly guess which block will no longer be used, but an effective strategy is to replace the
cache block that has the longest amount of time where it was not referenced to. This is also referred to as
the Least-Recently-Used \textit{(LRU)} algorithm.

\paragraph*{Write policy} \mbox{} \newline
Which is the policy that determines when changes that are written to the cache are written to
the main memory. This policy should find a balance between updating the main memory every time a block is written to
and updating the main memory only when a block is evicted from the cache.
The first extreme (updating when written to) is bad for performance whilst the second extreme (updating when evicting)
might cause interference with DMA and multi-processor operation \textit{(both are described later)}.

\subsection{I/O Access Techniques}

There are three techniques possible for I/O operations. They are listed below.

\subsubsection{Programmed I/O}
When a processor executes an instruction related to I/O, it issues a command to the relevant
I/O module. Then, it keeps checking the I/O module until the requested data is returned to the CPU.

\subsubsection{Interrupt-driven I/O}
When a processor executes an instruction related to I/O, it issues a command to the relevant
I/O module. Then, it does something else until it is notified by an interrupt from the I/O module
so that the processor can then receive the data from the I/O module.

Both programmed and interrupt-driven I/O suffer from two drawbacks:

\begin{itemize}
    \item The processor has to manage the I/O transfer and can thus not execute other instructions.
    \item The I/O transfer rate is limited by the speed of the CPU.
\end{itemize}

DMA (described below) can be used to counter these drawbacks.

\subsubsection{Direct Memory Access (DMA)}
DMA is best-used when multiple words of data are required to be transferred and involves
delegating the transfer to a DMA module.

When a processor executes an instruction related to I/O, it issues a command to the DMA module containing
the following information:

\begin{itemize}
    \item Whether the operation is a read operation or a write operation.
    \item The address of the target I/O device.
    \item The starting location in memory to read data from or write data to.
    \item The number of words to be read or written.
\end{itemize}

The DMA module then executes this command and notifies the processor using an interrupt when finished.

One drawback of DMA is that the DMA module needs to take control of the bus. If the CPU also needs
to use the bus, it has to yield to the DMA module. Nevertheless, DMA is still more efficient then other
I/O access techniques for multiple word I/O transfers.

\subsection{Multiprocessor and Multicore organization}
Computers have long been referred to as sequential machines. This was never entirely true in the first place,
as certain internal elements of the execution cycle have always been performed sequential instead of parallel.
Over time, computers included more and more features to perform work in parallel rather then sequentially.
Some of these approaches are \textbf{symmetric multiprocessors \textit{(SMP)}}, \textbf{multicore computers} and
\textbf{clusters}. The first two of these approaches are described below in more detail.

\tipbox{
    An SMP is a computer with multiple physical processor chips where a multicore computer has a processor
    die that has multiple physical processing cores.
}

\subsubsection{SMPs}
An SMP is a stand-alone computer system with the following characteristics:

\begin{itemize}
    \item The system contains two or more similar processors with a comparable feature set.
        \begin{itemize}
            \item These processors must share the main memory.
            \item These processors must share I/O facilities.
            \item These processors must be interconnected by an internal connection scheme (or bus)
                that ensure that memory access time for each processor is approximately equal.
            \item These processors must be able to perform the same functions. \textit{(Hence the name 'Symmetric'.)}
        \end{itemize}
    \item The computer system is controlled by a single integrated operating system.
        \begin{itemize}
            \item This OS must provide interaction between processors.
            \item This OS must provide interaction between running programs at the following levels:
                \begin{itemize}
                    \item Job;
                    \item Task;
                    \item File;
                    \item Data.
                \end{itemize}
        \end{itemize}
\end{itemize}

These systems offer the following \textit{potential} benefits over a system that is equipped with a single processor:

\begin{itemize}
    \item Increased performance which is achieved by performing work in parallel rather then sequentially.
    \item Availability, when a processor stops working another processor can take over.
    \item Upgradeability \textit{(incremental growth)}: the user can upgrade by adding another processor to the system.
    \item Scaling: Vendors can offer multiple system configurations with a varying amount of processors.
\end{itemize}

An example of the organization of a modern SMP system is shown in figure \ref{fig:1-smp-organization}. As is visible in the image,
each processor has at least one private cache. This causes an issue known as the \textbf{cache coherence problem}, where if a word
is changed in the cache of processor A, this might invalidate a cached value of the same word in processor B. To counter this,
processors must alert other processors when a word changes in their cache.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{figures/include/1-computer-system-overview/smp-organization}
    \caption{Example organization of an SMP system with 4 processors}
    \label{fig:1-smp-organization}
\end{figure}

\subsubsection{Multicore Computers}
Multicore computers, which processor chips are also known as \textbf{chip multiprocessors}, are computers where
multiple \textbf{processing cores} are combined on a single die. Virtually every modern PC CPU is a multicore processor.
Each processing core typically contains the following components that traditionally belonged to an independent processor:

\begin{itemize}
    \item Registers;
    \item \textbf{ALU}s \textit{(Arithmetic Logic Unit)};
    \item Pipeline hardware;
    \item Control unit;
    \item L1 instruction caches and data caches.
\end{itemize}

Dies often contain L2 and L3 caches that are shared between cores.

An example is the Intel® Core™ i7-5960X chip that has six x86 processing cores, with each an instruction L1 cache,
a data L1 cache, an L2 cache. The L3 cache is shared between all cores.

It contains a \textbf{DDR4 memory controller} \textit{(DDR4 = double data rate, version 4)} for interaction with the
main memory and provides a \textbf{PCI Express} \textit{(PCIe)} bus that enables high-bandwidth communication with
I/O devices and other connected chips.


