\section{I/O management \& disk scheduling}

\subsection{I/O devices}
I/O devices can be grouped into three general categories:

\begin{enumerate}
    \item \textbf{Human readable}: Suitable for communicating with the user.
        Examples are printers, displays and keyboards.
    \item \textbf{Machine readable}: Suitable for communicating with electronic
        equipment. Examples are sensors, SSDs and HDDs.
    \item \textbf{Communication}: Suitable for communicating with remote devices.
        Examples are 4G modems and network cards.
\end{enumerate}

All I/O devices have certain distinct properties:

\begin{itemize}
    \item \textbf{Data rate}: the number of bits transferred to and from the
        device per unit of time.
    \item \textbf{Application}: The usage of a device. This determines the type of
        software required by the OS to support it.
    \item \textbf{Complexity of control}: How complex the device is to control.
        A mouse is relatively easy to control when compared with a graphics card.
    \item \textbf{Unit of transfer}: The data transfer unit that is used when communicating.
        This could be a simple stream of bytes for a terminal or large blocks for storage I/O.
    \item \textbf{Data representation}: The data encoding scheme used when communicating
        with the device.
    \item \textbf{Error conditions}.
\end{itemize}

\subsection{OS design issues}

\subsubsection{Design objectives}
An I/O facility of an OS has two primary objectives. The first objective is \textbf{efficiency},
to ensure efficient system operation. Multiprogramming can aid in achieving this objective.
The second objective is \textbf{generality}: to ensure simplicity and a low error rate, it is
desired that all I/O devices are handled in a uniform way. This also eases the programming of
applications that use I/O devices. However, it is difficult to achieve true generality because of
the diverse landscape of I/O devices.

\subsubsection{Logical structure of the I/O function}
The simplest I/O function has the involves the following layers, starting
from the process of the user:

\begin{enumerate}
    \item \textbf{Logical I/O}: The logical I/O module that deals with the I/O device as a resource
        and is not concerned with actually controlling the device. It allows the process to invoke
        abstract I/O functions such as \texttt{write}, \texttt{open} and \texttt{close}.
    \item \textbf{Device I/O}: Converts the the operations requested from the logical I/O layer
        into appropriate sequences of I/O instructions, controller orders and channel commands. May
        use buffering techniques for optimization.
    \item \textbf{Scheduling and control}: Actually enqueues, schedules and controls I/O operations,
        handles interrupts and communicates with the I/O module and the hardware.
\end{enumerate}

For a communications device, the logical I/O layer is replaced with a \textbf{communications 
architecture} layer such as UDP. For a secondary storage device supporting a file system,
the logical I/O layer is replaced by the following three layers, starting from the
user process:

\begin{enumerate}
    \item \textbf{Directory management}: Converts symbolic file names to identifiers that
        either reference the file directly or through a file descriptor/index table. Handles
        more complex user operations such as \texttt{add}, \texttt{delete} and \texttt{move}.
    \item \textbf{File system}: Deals with the logical structure of files and primitive
        user operations such as \texttt{open}, \texttt{close}, \texttt{read} and \texttt{write}.
        Manages access rights.
    \item \textbf{Physical organization}: Converts logical references to files and records
        to physical secondary storage addresses, taking the physical track and sector
        structure of the storage device into account. Usually handles storage space allocation
        and buffers.
\end{enumerate}

\subsection{I/O buffering}
Buffering is the technique that performs input transfers in advance of requests being made
and that performs output transfers some time after the device is made by first writing
the data to a buffer. This prevents overhead such as process swapping because the process
no longer becomes blocked.

\subsubsection{Single buffer}
A single buffer is the simplest buffer: a buffer is created in main memory
that is used for reading from or writing to the device.

\subsubsection{Double buffer}
Double buffering is a technique where two buffers are used. As the processor
reads from one buffer, the other buffer can be filled by the I/O devices. Then,
the buffers are swapped.

\subsubsection{Circular buffer}
A circular buffer is similar to a double buffer but it involves more than
one buffer.

\subsubsection{Utility of buffering}
Buffers smooth out peaks in I/O demand. However, if the write rate from a process
is higher than the write rate of an I/O device, the buffers will eventually
fill up.

\subsection{Disk scheduling}
Disk scheduling is the scheduling of I/O requests to secondary memory.

\subsubsection{Disk performance parameters}
These are performance parameters for traditional HDDs, not SSDs. Apparently SSDs are still
not relevant for a book that was last revised in 2018. When a hard drive is requested to read
or write to/from a sector, the arm first moves tho the requested track. Then, it needs to wait
until the required sector is underneath the arm.

\paragraph*{Seek time} The \textbf{seek time} is the time it takes for the arm to move
onto the requested track.

\paragraph*{Rotational delay} The \textbf{rotational delay} is the time it takes for the
requested sector to rotate to a position underneath the arm.

\paragraph*{Transfer time} The \textbf{transfer time} is the time required to
actually transfer the data.

\begin{equation}
    \label{equ:10-transfer-time}
    T_t = \frac{b}{rN}
\end{equation}

\autoref{equ:10-transfer-time} shows the relation between the transfer time
and the other parameters of a hard drive, where:

\begin{itemize}
    \item $T_t =$ the transfer time.
    \item $b =$ the number of bytes to be transferred.
    \item $N =$ the number of bytes on a track.
    \item $r =$ the rotation speed in revolutions per second.
\end{itemize}

\paragraph*{Access time} The \textbf{access time} is the total time it takes
to read/write a set of sequential sectors from the disk.

\begin{equation}
    \label{equ:10-access-time}
    T_a = T_t + \frac{1}{2r} + T_s
\end{equation}

\autoref{equ:10-access-time} shows the definition of the access time, where:

\begin{itemize}
    \item $T_a =$ the access time.
    \item $T_s =$ the average seek time.
    \item $\frac{1}{2r} =$ the average rotational delay.
\end{itemize}

Because of the nature of HDDs, reading sequential sectors is significantly
faster than reading random sectors.

\subsubsection{Disk scheduling policies}

\paragraph*{First in, first Out (FIFO)} \mbox{} \newline
The simplest form of disk scheduling is FIFO scheduling, where a sequential queue is
used. Because the FIFO policy does not take the disk arm movement into account,
one can only hope for good performance if there are multiple processes active.

\paragraph*{Priority (PRI)} \mbox{} \newline
A form of disk scheduling where the priority of the underlying process is used
to schedule disk IO. This policy does not aim for the highest disk efficiency but
can be used to achieve other objectives of the OS.

\paragraph*{Last in , first out (LIFO)} \mbox{} \newline
The opposite to FIFO where a stack is used instead of a queue. The advantage is
that continually satisfying the last request requires less disk movement and thus
increase overall latency but it is still not guaranteed to perform good.

\paragraph*{Shortest service time first (SSTF)} \mbox{} \newline
A policy that prioritizes the lowest amount of disk arm movement. Performs
better than FIFO.

\paragraph*{SCAN} \mbox{} \newline
The SCAN policy operates like an elevator as it keeps the arm moving into
the same direction until either the last track is reached or there are no
more requests in that direction. The direction is then reversed and the policy
continues operating in the same fashion.

SCAN is similar to SSTF in terms of performance. However, it is biased
against the most recently traversed area (because it moves away) and thus
is not as good in exploiting locality as SSTF.

\paragraph*{Circular SCAN (C-SCAN)} \mbox{} \newline
C-SCAN is like SCAN but it restricts scanning in one direction, scans
until the end of the disk and then returns to the beginning of the disk
before starting to scan again. This reduces the maximum delay experienced
by new requests.

\paragraph*{N-step-SCAN and FSCAN} \mbox{} \newline
The N-step-SCAN policy segments the disk request queue into subqueues of length
$N$. Subqueues are then processed one at a time using SCAN. With large $N$ values,
the performance approaches the performance of SCAN. With $N=1$, the performance is
equal to the performance of FIFO.

FSCAN uses two subqueues, one queue where new requests are placed in and one
queue that is currently being processed.

These algorithms were designed to prevent a single process from monopolizing the
drive.
