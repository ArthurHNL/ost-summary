\section{File management}

\subsection{Overview}
\subsubsection{Files and file systems}
A file system permits users to create data collections known as
\textbf{files} that have the following properties:

\begin{itemize}
    \item \textbf{Long-term existence}: files are stored on secondary
        storage and do not disappear when a user logs off.
    \item \textbf{Shareable between processes}: files have names and
        can have associated permissions that permit controlled sharing.
    \item \textbf{Structure}: a file system has a certain structure
        such as a hierarchical folder structure and files can have an
        internal structure that can be convenient for applications.
\end{itemize}

File systems typically provide the following operations that can be performed
on files:

\begin{itemize}
    \item \textbf{Create}.
    \item \textbf{Delete}.
    \item \textbf{Open}: marks a file as "opened" by a process, allowing the
        process to perform functions on that file.
    \item \textbf{Closed}: inverse of open.
    \item \textbf{Read}.
    \item \textbf{Write}.
\end{itemize}

File systems usually maintain a set of attributes for each file that include
permissions and the owner.

\subsubsection{File structure}
Files consists of \textbf{records} which in turn consist of \textbf{fields}. Fields
are the smallest possible units that can be saved.

\subsubsection{File management systems}
A file management system is a set of system software providing services to users
and applications in the use of files. A file management system has the following
objectives:

\begin{enumerate}
    \item Meet the data management needs and requirements of the user, including
        the storage of data and the ability to perform the operations mentioned above.
    \item Guarantee to a reasonable extent that the data in the files is valid.
    \item Optimize performance from both the system's and the user's point of view.
    \item Provide I/O support for various storage device types.
    \item Minimize or eliminate the potential for lost or destroyed data.
    \item In case of multiple user systems, provide I/O support for multiple users.
\end{enumerate}

The following requirements should be fulfilled to achieve the first objective
on an interactive general-purpose system:

\begin{enumerate}
    \item Each user must be able to create, delete, read, write and modify files.
    \item Each user may have controlled access to the files of another user.
    \item Each user may control what types of accesses are allowed to the user's files.
    \item Each user should be able to move data between files.
    \item Each user should be able to access his/her files by name.
    \item Each user should be able to backup and restore his/her files.
\end{enumerate}

\paragraph*{File system architecture} \mbox{} \newline
Usually, file systems have a layered architecture. This starts with
\textbf{device drivers} at the lowest level. These are controlled by
the \textbf{basic file system} which deals with blocks of data. Above that level,
the \textbf{basic I/O supervisor} handles I/O initiation and termination. This level
is controlled by the \textbf{logical I/O} layer which enables users and applications
to access records. An \textbf{access method} sits between this layer and the user program.

\subsection{Secondary storage management}
On secondary storage, a file is merely a collection of data blocks. The OS or
file management system is responsible for allocating blocks to files. This implies that
there must be a way to allocate storage to files and that there must be a way to
keep track of which blocks are allocated to which file.

\subsubsection{File allocation}
File allocation has to resolve the following issues that are listed below.

\paragraph*{Preallocation versus Dynamic allocation} \mbox{} \newline
Preallocation policies require the maximum size of a file to be declared
when it is created and the space for it to be allocated. Dynamic allocation
allocates more secondary memory to a file when it is needed.
Dynamic allocation is more flexible but causes more fragmentation.

\paragraph*{Portion size} \mbox{} \newline
The \textbf{portion size} is the size of contiguous allocated blocks
that are allocated to the same file. Contiguity increases performance
because the disk does not have to seek as much. Large portions also
shrink the size of tables needed to keep track of the portions.
Fixed-sized portions reduce management overhead but variable-sized portions
reduce waste of unused allocated memory.

There are two major alternatives: \textbf{variable, large contiguous portions} that
deliver the most performance but leave empty space that is hard to reuse and
\textbf{blocks}: small fixed portions that provide greater flexibility at the cost
of performance.

It is not clear whether it is best to use a first fit, best fit or nearest fit
policy when allocating variable, large contiguous portions of secondary memory.

\paragraph*{File allocation methods} \mbox{} \newline
There are three ways to allocate secondary memory to files:

\begin{itemize}
    \item \textbf{Contiguous allocation}: files are allocated as a
        single contiguous set of blocks. The file table maintains a pointer
        to the starting block and the length.
    \item \textbf{Chained allocation}: files are allocated random blocks.
        Each block contains a pointer to the next block. The file table maintains a
        pointer to the starting block and the length.
    \item \textbf{Indexed allocation}: each file is allocated an index block that contains
        pointers to the other file blocks. The file table maintains a pointer to the
        index block.
\end{itemize}

\subsubsection{Free space management}
The system must also keep record of free space in order to know where to
allocate memory for new files. A number of solutions for this are listed below.

\paragraph*{Bit Tables} \mbox{} \newline
A vector that contains a bit for each storage block is maintained; the bit
is set to 0 when the storage block is free and to 1 when the storage block is in use.

\paragraph*{Chained free portions} \mbox{} \newline
The system maintains a table to the first free memory block. Each free memory
block contains a pointer to the next free memory block. This method has negligible
space overhead but does increase fragmentation.

\paragraph*{Indexing} \mbox{} \newline
The system treats free space as a file and maintains it in a file index.
This works best when working with variable-sized portions because there will then
be one entry in the file table for every empty memory portion.

\paragraph*{Free block list}
Each block is assigned a block number sequentially and a list containing
all the free blocks is maintained in a reserved portion on the disk.

\subsubsection{Volumes}
A volume is a collection of addressable sectors in secondary memory that an OS
or an application can use for data storage. Frequently, a disk is divided into partitions.
Each partition can then function as a separate volume.

\subsubsection{Reliability}
To reduce the risk of file table corruption, file table should be protected
by a locking mechanism when storage is being allocated or deallocated.

\subsection{UNIX file management}
The UNIX file system has six types of files:

\begin{itemize}
    \item \textbf{Regular}: Contain arbitrary data in zero or more data blocks.
    \item \textbf{Directory}: Contains a list of file names together with pointers
        to the associated inodes. Directory are hierarchically organized
        and are protected in a way that only the system can write to them.
    \item \textbf{Special}: contain no data but provide a mechanism to map
        physical devices to file names.
    \item \textbf{Named pipes}: an IPC facility that buffers data.
    \item \textbf{Links}: An alternative name for an existing file.
    \item \textbf{Symbolic link}: A file that contains the name of a file to which
        it is linked.
\end{itemize}

\subsubsection{Inodes}
Inodes (index nodes) are control structures used by the OS to administer files. They
typically contain the following information

\begin{itemize}
    \item The access mode and type of the file.
    \item The file's owner and group-access identifiers.
    \item A timestamp for the creation time, most recent read time, most recent write time
        and most recent inode update.
    \item The size of the file.
    \item A sequence of block pointers.
    \item The number of physical blocks used by the file.
    \item The number of directory entries that reference the file.
    \item The kernel and user-settable flags that describe characteristics of the file.
    \item The generation number of the file.
    \item The block size of the data blocks referenced.
    \item The size of extended attribute information.
    \item Zero or more extended attributes entries.
\end{itemize}

\subsubsection{File allocation}
File allocation is done dynamically on a block basis. An index method is used
to keep track of each file. Part of the index is stored in the inode for the file.

\subsubsection{Volume structure}
A UNIX volume resides on a single logical disk (partition) and is laid out
with the following elements:

\begin{itemize}
    \item \textbf{Boot block}: the code required to boot the OS.
    \item \textbf{Superblock}: contains attributes about the file system such as
        the inode table size.
    \item \textbf{Inode table}.
    \item \textbf{Data block}: the available storage space.
\end{itemize}
