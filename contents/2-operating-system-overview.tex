\section{Operating System Overview}

\subsection{OS Purpose and Functionality}
An \textbf{Operating System} \textit{(OS)} is a piece of system software running on a computer that acts
as interface between soft- and hardware and controls execution of applications. It serves three purposes that
are stated and detailed below.

\subsubsection{Convenience by providing a common hardware interface, services and tooling}
The OS offers the following services and tooling, that all provide convenience by abstracting away
implementation details for the user:

\begin{itemize}
    \item \textbf{Instruction Set Architecture} \textit{(ISA)}: Defines the set of machine instructions that
        a computer can follow and thus serves as boundary between hardware and structure. A subset of the
        entire ISA or \textbf{system ISA} is exposed to applications. This subset is named the \textbf{user ISA}.
    \item \textbf{Application Binary Interface} \textit{(ABI)}: Defines the binary \textbf{system call} \textit{(syscall)}
        interface to the OS, services and hardware resources available in the user ISA.
    \item \textbf{Application Programming Interface} \textit{(API)}: Gives a program(mer) access to resources of the
        user ISA by providing libraries for high-level programming languages. Allows software to be easily ported
        to another system that implements the same API.
    \item \textbf{Program execution}: Performs activities necessary for the execution of programs including but
        not limited to scheduling and loading instructions into memory.
    \item \textbf{User Interface} \textit{(UI)}: The OS can provide a UI to the user such as a desktop environment.
        This is usually an application that is bundled with the OS but not a core part of the OS.
    \item \textbf{Application development tools}: The OS can provide tooling that is used to developed software
        such as text editors and debuggers. Just like the UI, these tools are bundled with, but not a core part of, the OS.
    \item \textbf{I/O Access}: Provides a uniform interface for accessing I/O devices.
    \item \textbf{File Access}: Provides a uniform interface for accessing files on different storage media,
        including access control.
    \item \textbf{Error handling}: Detects errors (both HW and SW) during program execution and resolves these errors
        with as less impact as possible.
    \item \textbf{Accounting}: Calculates and provides usage statistics.
\end{itemize}

The first three bullet points are illustrated in figure \ref{fig:2-hw-sw-structure}.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.75\textwidth]{figures/include/2-operating-system-overview/sw-hw-structure}
    \caption{Hardware \& Software structure of a computer system}
    \label{fig:2-hw-sw-structure}
\end{figure}

\subsubsection{Efficiency by managing resources}
The OS acts as a resource manager for system resources but is also dependent on the resources that it
is managing because in the end it is just another program that the processor is executing. Therefore, the OS must
depend on the processor to regain control when control is needed. The mechanisms the OS uses to achieve this will
be discussed later.

The lowest, most frequently used part of the OS named the \textbf{kernel} is always present in main memory. The OS
manages resources like I/O devices, CPU time and memory.

\subsubsection{Ease of evolution}
The operating system provides ease of evolution by providing updates that include support for new hardware,
bugfixes and extra features.


\subsection{Major concepts of an OS}
OSes are the among the most complex pieces of software that have ever existed. This section
discusses four major concepts that are relevant when dealing with operating systems. 

\subsubsection{Processes} \label{2-process}
A \textbf{process} is a construct, just like colour, and has multiple definitions:

\begin{itemize}
    \item A program in execution.
    \item A unit that can be assigned to and executed by a processor core.
    \item A unit of an activity that exists of:
        \begin{itemize}
            \item A set of processor instructions.
            \item A state.
            \item A set of resources that are currently owned by the process.
        \end{itemize}
\end{itemize}

The need of the concept of a process came from three problems that arose in the lines of OS development:

\begin{itemize}
    \item \textbf{Multiprogramming batch-operation}: Multiprogramming was designed to efficiently use the
        system resources of a computer system and there thus must be a way to shift resources between executing
        programs.
    \item \textbf{General-purpose time sharing}: This is needed to be both responsive to an individual user and
        serve multiple users at the same time to save costs.
    \item \textbf{Real-time transaction processing}: Where multiple programs need to update multiple resources in a
        single atomic transaction. 
\end{itemize}

Before processes were introduced, these problems were usually worked around using interrupts. However this introduced
a lot of bugs in software, of which most fit in to one of the following categories:

\begin{itemize}
    \item \textbf{Improper synchronization}: When a routine must be suspended awaiting an event from another
        system component and this synchronization is not properly implemented.
    \item \textbf{Failed mutual exclusion}: When two concurrently use a shared resource in a way that
        corrupts the resource.
    \item \textbf{Indeterminate program operation}: When programs share memory and interfere with each others
        values in memory, resulting in nondeterministic behaviour of programs.
    \item \textbf{Deadlocks}: When two (or more) routines are both waiting for each other to finish, signal,
        or release a resource. Eg. when four cars arrive at a Dutch four-way intersection and non of them
        want to turn.
\end{itemize}

The implementation of a process in an OS is usually a data structure consisting of the following components:

\begin{itemize}
    \item The \textbf{executable program}.
    \item The \textbf{data needed by the program}, like buffers and variables.
    \item The \textbf{execution context} of the program (also know as \textbf{process state}). This contains internal data
        of the OS, that ius not accessible to the process itself, either for security reasons or because it is not relevant.
        This includes de state of the processor registers and other metadata.
\end{itemize}

This structure allows operating systems to efficiently schedule and coordinate processes.

\subsubsection{Memory management}
An operating system has to fulfil certain requirements regarding memory management. It must
support modular programming, flexible use of data and provide orderly control of storage allocation.
To fulfil these requirements, an OS has five principles with regard to memory management. These principles
are listed below.

\begin{enumerate}
    \item \textbf{Process isolation}.
    \item \textbf{Automatic allocation and management}: program memory should be dynamically allocated
        across the memory hierarchy as required.
    \item \textbf{Support of modular programming}.
    \item \textbf{Long-term storage}.
\end{enumerate}

These principles are usually implemented using \textbf{virtual memory} and using \textbf{file systems}, with the
latter being focused on long-term storage. Virtual memory allows programs to address memory from a
logical point of view, without having to care about how much main memory is available. The program is given
a virtual address that the \textbf{memory management unit} of the OS then maps to the real address of a value
when required. This allows the OS to move the memory block of a process to other regions of memory or store it
on another place in the memory hierarchy and the program itself does not have to deal with this.


\subsubsection{Information security and protection}
Operating systems provide the following categories of general-purpose tool to ensure
that data is secure and protected:

\begin{itemize}
    \item \textbf{Availability}: protect the system against interruptions.
    \item \textbf{Confidentiality}: ensure that data can only be read with sufficient authorization.
    \item \textbf{Data integrity}: ensure that data can only be written to with sufficient authorization.
    \item \textbf{Authenticity}: validate that users and data are authentic.
\end{itemize}

\subsubsection{Resource Management \& Scheduling}
An OS must manage resources available on the system and schedule when they are used. Any
\textbf{resource allocation and -scheduling policy} must keep the following factors in mind:

\begin{itemize}
    \item \textbf{Fairness}: give each process competing for a specific resource the same
        equal access.
    \item \textbf{Differential responsiveness}: Discriminate among classes of jobs to increase
        overall efficiency.
    \item \textbf{Efficiency}: Maximize throughput and minimize response time.
\end{itemize}

Scheduling is usually done using a set of queues:

\begin{itemize}
    \item A long-term queue that contains processes not in main memory.
    \item A short-term queue that contains processes in main memory.
    \item A queue for each IO device of the system.
\end{itemize}

A new process is always placed in the long-term queue. At a certain moment, the process is placed in the
short term queue and then loaded into main memory. Then, the process is taken from the short term queue
and starts execution. Should it need to access an IO device, it shall be placed in the relevant IO queue.

\subsection{OS Evolution}
This section lists several important OS evolutions that together lead to the modern operating systems
we all know today.

\subsubsection{Microkernel architecture}
Until recently OSes usually had a \textbf{monolithic kernel} that implemented everything from low-level memory
management up to networking and was usually implemented as a single process. Nowadays, most OSes feature a
\textbf{microkernel} architecture where only a small kernel implements the OS most vital functions such as scheduling
and memory management. Other services are provided by processes that run in user mode, sometimes named \textbf{servers}.

\subsubsection{Multithreading}
Multithreading is a technique that allows a single process to have multiple \textbf{threads}, which is a dispatchable unit of work
that can be executed and has an own processor context with a program counter and a stack pointer.

\subsubsection{SMP}
SMPs allows OSes to schedule threads over multiple processing cores, increasing throughput.

\subsubsection{Distributed OSes}
Distributed operating systems can provide a virtual memory space and other unified access facilities for
multiple computers.

\subsubsection{Object-oriented design}
OO design enables programmers to customize an operating system without disrupting its integrity.

\subsection{Case example: Microsoft Windows \texorpdfstring{\faIcon[regular]{windows}}{}}
Windows started as an extension to MS-DOS in 1985 but the first Windows that had its own kernel,
Windows  NT, shipped in 1993. The latest current release is Windows 10 and runs mainly on PCs.

\subsubsection{Architecture}

The internal architecture of Windows is displayed in figure \ref{fig:2-win32-architecture}. The components will
be shortly explained below the figure. The architecture is highly modular and is client/server based in order to
simplify the executive layer, improve reliability and lay down foundations for distributed computing. Windows supports
SMP and multithreading.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{figures/include/2-operating-system-overview/win32architecture}
    \caption{Windows Internal Architecture}
    Coloured areas are executive.
    \label{fig:2-win32-architecture}
\end{figure}

\paragraph*{Kernel mode layers}
\begin{enumerate}
    \item The \textbf{Executive} layer contains the core services of the OS.
    \item The \textbf{Kernel} layer controls processor execution.
    \item The \textbf{Hardware Abstraction Layer (HAL)} abstracts away different hardware types
        for the executive layer.
    \item The \textbf{Device \& Filesystem drivers} extend the executive layer by translating I/O requests
        into I/O requests that I/O hardware is able to interpret.
\end{enumerate}

\paragraph*{User mode process types}
\begin{enumerate}
    \item \textbf{Special system support processes} such as Windows Explorer that manage the system.
    \item \textbf{Service processes} that run in the background such as an RDP service.
    \item \textbf{Environment subsystems} that provide different OS personalities. Win32 and POSIX are supported.
    \item \textbf{User applications} which are regular programs (.exe).
\end{enumerate}

\subsubsection{Windows Objects}
Within Windows, object-oriented design is applied for entities that are intended for user-mode access,
are shared and/or are restricted. These involve files and semaphores. The object manager is responsible
for creating these objects and managing their lifespan. Objects may or may not have names and may have a
security descriptor that manages the access to the object.

\pagebreak

\subsection{Case example: Traditional UNIX systems}
The initial UNIX release was released in 1970 by Bell Labs. It was the first OS to be
completely rewritten in C.

\subsubsection{Architecture}
The traditional UNIX kernel architecture is shown in figure \ref{fig:2-unixtraditional-architecture}. It
does not support multithreading or SMP and consists of three levels: user, kernel and hardware. The traditional
UNIX kernel was never designed to be extensible and has no features for code reuse. This results in a bloated
and non-modular kernel.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\textwidth]{figures/include/2-operating-system-overview/unixtraditionalarchitecture}
    \caption{Traditional UNIX internal architecture}
    \label{fig:2-unixtraditional-architecture}
\end{figure}

\pagebreak

\subsection{Definition of a kernel}
A kernel can be seen as the core of the operating system. A modern kernel provides the
following functionality:

\begin{itemize}
    \item Process management:
        \begin{itemize}
            \item Process creation \& termination
            \item Process scheduling \& dispatching
            \item Process switching
            \item Process synchronization
            \item Facilitating interprocess communication
            \item Management of process control blocks
        \end{itemize}
    \item Memory management:
        \begin{itemize}
            \item Allocation of address space to process
            \item Swapping
            \item Page management
            \item Segment management
        \end{itemize}
    \item I/O management:
        \begin{itemize}
            \item Buffer management
            \item Allocating I/O devices and I/O channels to processes
        \end{itemize}
    \item Support functions:
        \begin{itemize}
            \item Interrupt handling
            \item Accounting
            \item Monitoring
        \end{itemize}
\end{itemize}

\pagebreak

\subsection{Case example: Modern UNIX systems}
UNIX evolved into an ecosystem of modern implementations which each have their own characteristics.

\subsubsection{Architecture}
A typical modern UNIX kernel architecture is displayed in figure \ref{fig:2-unixmodern-architecture}. A small
modular core of facilities facilitates the implementation of interfaces in the outer circle.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.75\textwidth]{figures/include/2-operating-system-overview/unixmodernarchitecture}
    \caption{Modern UNIX internal architecture}
    Coloured areas are executive.
    \label{fig:2-unixmodern-architecture}
\end{figure}

\pagebreak

\subsection{Case example: Linux \texorpdfstring{\faIcon[regular]{linux}}{}}
The Linux kernel is a free and open source UNIX implementation that Linus Torvalds, the initial developer of Linux
posted on the internet in 1991. Nowadays it runs on virtually every platform.

\subsubsection{Architecture}
\paragraph*{Modules} \mbox{} \newline
Instead of being monolithic like most UNIX kernels, the Linux kernel has a highly modular architecture that allows
certain modules to be reloaded without requiring a full system reboot. Note that Linux does not have a microkernel
architecture.
Linux is structured as a collection of \textbf{loadable modules} which are object files.
Each module implements a specific function and expose their functionality in the form of symbols
in the \textbf{symbol table} of the module An example for a symbol would be FAT.
Modules have two important characteristics:

\begin{enumerate}
    \item \textbf{Dynamic linking}: Linux kernel modules can be loaded and linked to and unloaded and unlinked
        from an already running kernel.
    \item \textbf{Stacking}: Linux kernel modules are arranged in a hierarchy. They serve as libraries when referenced
        by modules higher up the hierarchy and serve as clients when referenced by modules further down the hierarchy.
\end{enumerate}

\paragraph*{Kernel Components} \mbox{} \newline
The components of the linux kernel can be placed in the following categories:

\begin{enumerate}
    \item \textbf{Signals}: Signals are used by the kernel to call into the process, for example to notify
        that a fault has occurred.
    \item \textbf{System calls}: Syscalls are used by processes to request a kernel service.
    \item \textbf{Processes and scheduler}: Manages processes and scheduling.
    \item \textbf{Virtual memory}: Manages virtual memory for processes.
    \item \textbf{File systems}.
    \item \textbf{Networking}.
    \item \textbf{Device drivers}.
    \item \textbf{Traps and fault handlers}: handle traps and faults from the CPU, usually by sending signals to
        the relevant process.
    \item \textbf{Interrupt handlers}: handles interrupts from the CPU.
\end{enumerate}
