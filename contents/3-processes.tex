\section{Processes}
This section further describes the concept of a process. The definition of a
process is described in \ref{2-process} \nameref{2-process}. A process is
program code combined with associated data and a process control block.

\subsection{Process states}
Processes are dispatched by a \textbf{dispatcher} program which handles
a queue of processes. The simplest process state model is a model with two
states that is either running or not running. Most modern operating systems
operate with a seven state model which is shown in figure \ref{fig:3-7-state-process-model}.
Each state, except the new, exit and running state has an associated queue that the
dispatcher uses to dispatch the next relevant process. The states and transitions
will be explained below.

Processes can be \textbf{suspended} (or \textbf{swapped}) to save memory.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\textwidth]{figures/include/3-processes/process-states}
    \caption{7-state process model}
    \textit{Any state except the Exit state can transition to the exit state.}
    \label{fig:3-7-state-process-model}
\end{figure}

\paragraph*{States}
\begin{itemize}
    \item \textbf{Active \& Blocked}: The process is loaded into main memory
        and cannot be immediately executed because it is awaiting an event.
    \item \textbf{Active \& Ready}: The process is loaded into main memory
        and can immediately execute when dispatched.
    \item \textbf{Active \& Running}: The process is currently executing.
    \item \textbf{Exit}: The process has been released because it faulted,
        halted, was killed or stopped for another reason. The process can no
        longer be executed but the OS may still have to do some cleaning up.
    \item \textbf{New}: The process has just been created. It is assigned
        an identifier but not yet ready to be executed because the OS might
        still have to do some preparation work.
    \item \textbf{Suspended \& Blocked}: The process is not loaded into main memory
        and cannot be immediately executed because it is awaiting an event.
    \item \textbf{Suspended \& Ready}: The process is not loaded into main memory but
        can immediately execute when loaded into main memory.
\end{itemize}

\paragraph*{Transitions}
\begin{itemize}
    \item \textbf{Activate}: Suspended \& Blocked \textrightarrow{} Active \& Blocked:
        A rare transition that sometimes occurs when main memory becomes available and a high
        priority process is suspended and blocked. The process is moved from secondary memory
        to main memory by the OS.
    \item \textbf{Activate}: Suspended \& Ready \textrightarrow{} Active \& Ready:
        No Active \& Ready process are available but there are resources available for execution
        thus the OS decides to move a Suspended \& Ready process from secondary memory to main
        memory.
    \item \textbf{Admit}: New \textrightarrow{} Active \& Ready: Housekeeping duties for starting the
        execution of a new process are performed and the process is placed into main memory. 
    \item \textbf{Admit}: New \textrightarrow{} Suspended \& Ready: Housekeeping duties for starting the
        execution of a new process are performed and the process is placed into secondary memory. 
    \item \textbf{Dispatch}: Active \& Ready \textrightarrow{} Active \& Running: A process that
        is already in main memory starts execution.
    \item \textbf{Event occurs}: Active \& Blocked \textrightarrow{} Active \& Ready: A process that was waiting
        for an event received the required event and can now resume execution.
    \item \textbf{Event occurs}: Suspended \& Blocked \textrightarrow{} Suspended \& Ready: A process that was waiting
    for an event received the required event and can now resume execution after it is loaded into main memory.
    \item \textbf{Event wait}: Active \& Running \textrightarrow{} Active \& Blocked:
        The process requested something for which it must wait for an event, such as a syscall.
        The OS stops executing the process and executes another process that is currently in the Ready
        state.
    \item \textbf{Release}: \textit{Any state} \textrightarrow{} Exit: The process has halted,
        faulted or exited for another reason. The OS stops execution and moves it to the exit state
        so that it can later clean up the resources.
    \item \textbf{Suspend}: Active \& Blocked \textrightarrow{} Suspended \& Blocked:
        The OS makes room in main memory for processes that are ready when there are no
        ready processes in main memory. The process is moved to secondary memory
        to wait for an event.
    \item \textbf{Suspend}: Active \& Ready \textrightarrow{} Suspended \& Ready:
        The OS needs to free resources and no blocked process is in the active state so the OS
        moves an active ready process from the main memory to the secondary memory.
    \item \textbf{Suspend}: Active \& Running \textrightarrow{} Suspended \& Ready:
        Because of an urgent need of system resources, the OS suspends a currently
        running process and moves it from main memory to secondary memory.
    \item \textbf{Time out}: Active \& Running \textrightarrow{} Active \& Ready:
        The process has exceeded its currently allocated runtime, another process
        with a higher priority has been scheduled or the process voluntarily yields
        its resources. As a result the OS stops executing the process but leaves it
        in main memory.
\end{itemize}

\pagebreak

\subsection{Process Description}
The OS must maintain a certain internal data structures in order to efficiently manage
processes and the resources they require. They are categorized and listed below.

\subsubsection{OS Control Structures}
OS Control Structures keep track of all the resources that the OS is managing.

\paragraph*{Memory tables} \mbox{} \newline
Memory tables keep track of all the memory available to the OS. They contain the
following Information:

\begin{itemize}
    \item Allocation of main memory to processes.
    \item Allocation of secondary memory to processes.
    \item Information regarding memory security such as which process is allowed
        access to which memory part.
    \item Virtual memory management information.
\end{itemize}

\paragraph*{I/O tables} \mbox{} \newline
I/O tables keep track of I/O devices and channels of the system and their current
state.

\paragraph*{File tables} \mbox{} \newline
File tables keep track of files  in secondary memory.

\paragraph*{Process tables} \mbox{} \newline
Process tables keep track of the currently existing processes.

\subsubsection{Process Control Structures}
Process control structures are data structures that are used by an OS to control
processes.

\paragraph*{Process image} \mbox{} \newline
The process image is a data structure in memory that contains everything of the process. It
contains the following structures:

\begin{itemize}
    \item \textbf{User Data}: The memory allocated to the process.
    \item \textbf{User Program}: The program that is to be executed. May not be modified.
    \item \textbf{Stack}: A LIFO stack used to store parameters and calling addresses
        for procedure- and syscalls.
    \item \textbf{Process Control Block}: Data required by the OS to manage the process.
        Usually contains the following information:
        \begin{itemize}
            \item Unique numeric identifiers of:
                \begin{itemize}
                    \item This process.
                    \item The parent of the process.
                    \item The user identifier.
                \end{itemize}
            \item Processor state information including:
                \begin{itemize}
                    \item CPU registers:
                        \begin{itemize}
                            \item User registers.
                            \item Control and status registers including the program counter,
                                result of most recent operations and status information such as
                                the execution mode.
                        \end{itemize}
                \end{itemize}
            \item Process control information:
                \begin{itemize}
                    \item Process state.
                    \item Priority.
                    \item Scheduling metadata.
                    \item Events.
                \end{itemize}
            \item Pointers to related data structures.
            \item Interprocess Communication data.
            \item Privileges of the process.
            \item Memory management data.
            \item Resource handles.
        \end{itemize}
\end{itemize}

\subsection{Process Control}

\subsubsection{Process Execution Modes}
The processor has a register that contains the current execution mode
of the processor. This value indicates the current privilege level, with
0 being the highest privilege.

When this value is incremented, the currently
executing code can no longer interfere with the OS and the key tables of the OS.

In the event of an interrupt or a syscall, this value is cleared by the processor
so it will run in the most privileged mode to let the OS handle the interrupt
or syscall. After the syscall or interrupt, the value of the register is restored
so that the OS is protected.

\subsubsection{Process Creation}
The process of creating a new process consists of the following steps:

\begin{enumerate}
    \item Assign an identifier to the process.
    \item Allocate space for the process image.
    \item Initialize the process control block.
    \item Set appropriate linkages such as scheduling queues.
    \item Create and/or expand other data structures.
\end{enumerate}

\subsubsection{Process Switching}

\paragraph*{Reasons to switch processes} \mbox{} \newline
Several reasons exists to switch processes. They are listed below.

\subparagraph*{Interrupts} happen for certain reasons such as \textbf{time interrupts}
when the maximum clock time for the process has expired, \textbf{I/O interrupts} to notify
that an I/O action has occurred and \textbf{memory fault interrupts} such as segmentation
faults. In this case, the control is switched to an interrupt handler of the OS running in
privileged mode. The OS might just notify the process, switch processes or terminate the
process and then switch processes.

\subparagraph*{Traps} occur when the OS encounters an error. If the error is fatal,
the process is terminated and the process is switched.

\subparagraph*{Supervisor calls} occur on explicit request of a process, such as a
syscall to open a file. This usually ends with the process being placed into blocked
state and the process being switched.

\paragraph*{Procedure of switching processes}
\begin{enumerate}
    \item Save the processor state information to the process control block.
    \item Update other relevant parts of the process control block such as
        the state of the process.
    \item Move the process to the appropriate dispatcher queue.
    \item Select another process for execution.
    \item Update the process control block of the selected process.
    \item Update relevant memory management structures.
    \item Load the processor state information from the process control block
        of the selected process into the processor.
\end{enumerate}

\subsection{Execution of the OS itself}
This section state several methods of executing the operating system itself.

\paragraph*{Out of process kernel execution} is a traditional approach where
a context switch is performed on every interrupt or supervisor call. After the
kernel is done, the process is switched back.

\paragraph*{Within user process OS execution} is a more modern approach where
interrupts and supervisor calls are handled/executed within the same process
but in privileged mode. This increases performance because it prevents at least
one context switch.

\paragraph*{Within own process OS execution} is an approach where most of the OS
except the process switching functions are implemented in separate processes
that run in privileged mode.
