\section{Synchronization mechanisms}
Synchronization mechanisms provide ways to write concurrent code in
which no race conditions occur. This section lists some of the more
popular synchronization mechanisms.

Most concurrency mechanism work
by only letting a set amount of threads access a given resource. The
code that uses a shared resource and must be guarded by a
synchronization mechanism is called a \textbf{critical section}.

\subsection{Semaphores}
A \textbf{semaphore} is an object that contains an integer value and has
two methods: \texttt{semWait} and \texttt{semSignal}. A semaphore
has the following properties:

\begin{itemize}
    \item The semaphore must be initialized with a non-negative value.
    \item Any thread/process calling the \texttt{semWait} method
        decrements the integer value of the semaphore by one. If the
        value becomes negative, the thread/process is blocked and must
        wait for a signal.
    \item Every time the \texttt{semSignal} method is called, the
        integer value of the semaphore is incremented by one. If the
        value becomes zero, a blocked process/thread is given an
        event and becomes ready.
\end{itemize}

The most general semaphore described above is also called a
\textbf{general semaphore}. There are also more specialized
semaphores:

\begin{itemize}
    \item \textbf{Binary semaphores} are semaphores that can only be
        initialized to 0 or 1.
    \item \textbf{Mutexes} are binary semaphores of which the caller of
        \texttt{semWait} must also be the caller of \texttt{semSignal}.
        This allows easy implementation of \textbf{mutual exclusion},
        where only one critical section is allowed access to a resource
        at a time. This is also called obtaining and releasing a lock.
\end{itemize}

A \textbf{strong semaphore} has a FIFO queue for which process/thread is
first unblocked when \texttt{semWait} is called.
A \textbf{weak semaphore} provides no guarantee about the order in
which processes/threads are unblocked.

Semaphores can be used to solve the \textbf{producer/consumer} problem,
a problem where a single consumer consumes data from a buffer that is
continually filled by one or more producers.

Semaphores are usually implemented using hardware support because
they do require a synchronization mechanism of their own to prevent
a race condition in the event that multiple callers call a method at the
same time.

\subsection{Monitors}
The \textbf{monitor} is a programming construct that provides equivalent functionality
to a semaphore but is easier to use. Most programming languages that support
multithreading also provide some form of monitor. For example, C\# has the
static \texttt{Monitor} class and the \texttt{lock} statement that makes using
this class even easier by introducing some compiler code generation magic.

Generally a monitor can lock a specific object variable, only allowing a single
thread to access it at the same time. This provides mutual exclusion. It also
provides more semaphore-like functionality with \texttt{wait}, \texttt{pulse} and
\texttt{pulseAll} methods. The first method waits if another call to this method has
already been made. The second method lets one thread that has called \texttt{wait}
access the object and the last method lets all threads that have called \texttt{wait}
access the objects.

\subsection{Message passing}
Message passing is a way for synchronization without shared memory. A producer
produces data and sends this directly or indirectly to a consumer which then processes
the data. This can also be implemented as a distributed system.

Messages can be directly addressed to a receiver or can go through a named mailbox
(or channel). Both the send and receive operation can be blocking. If the send
operation is blocking, the sender remains blocked until the receiver has acknowledged
that the message has been processed. Commonly, send is not blocking but receive is.

Message passing can be used to enforce mutual exclusion.

\subsection{The readers/writers problem}
The readers/writers problem is a problem regarding the following constraints
for accessing a data structure:

\begin{enumerate}
    \item Readers may concurrently read the data structure.
    \item Only one writer may write to the file at a given point in time.
    \item When a writer is writing, readers may not read the data structure.
\end{enumerate}

This problem is different from the general mutual exclusion problem because not
all participants in the problem are equal. The readers/writers problem is also
different from the producer/consumer problem because it does not involve a buffer
or a queue.

Two solutions are listed below.

\subsubsection{Solution where readers have priority}
This is the most simple solution.
A counter variable \texttt{readCount} is used to keep track of the amount of
readers, a mutex \texttt{readCountMut} is used to protect the \texttt{readCount}
counter variable. A binary semaphore \texttt{writeSem} is used to protect
the data structure from concurrent writing and concurrent reading and writing.

\paragraph*{Procedure for reading}
\begin{enumerate}
    \item Obtain a lock on \texttt{readCountMut}.
    \item Increment \texttt{readCount} by one.
    \item IF \texttt{readCount} is equal to one, then:
        \begin{enumerate}
            \item Call \texttt{semWait} on \texttt{writeSem}.
        \end{enumerate}
    \item Release the lock on \texttt{readCountMut}.
    \item Execute the reading operation.
    \item Obtain a lock on \texttt{readCountMut}.
    \item Decrement \texttt{readCount} by one.
    \item IF \texttt{readCount} is equal to zero, then:
        \begin{enumerate}
            \item Call \texttt{semSignal} on \texttt{writeSem}.
        \end{enumerate}
    \item Release the lock on \texttt{readCountMut}.
\end{enumerate}

\paragraph*{Procedure for writing}
\begin{enumerate}
    \item Call \texttt{semWait} on \texttt{writeSem}.
    \item Execute the writing operation.
    \item Call \texttt{semSignal} on \texttt{writeSem}.
\end{enumerate}

\pagebreak

\subsubsection{Solution where writers have priority}
This solution is more complex but it gives writer priority which might
be a design requirement.

A counter variable \texttt{readCount} is used to keep track of the number
of readers and a counter variable \texttt{writeCount} keep track of the number
of writers that want to write. The \texttt{readCountMut} and \texttt{writeCountMut}
mutexes protect these variables respectively. A binary semaphore \texttt{readSem} is
used to control access for reading and another binary semaphore \texttt{writeSem} is
used to control access for writing. Another mutex \texttt{readSemMut} ensure
that only one reader at a time interacts with \texttt{readSem}.

\paragraph*{Procedure for reading}
\begin{enumerate}
    \item Obtain a lock on \texttt{readSemMut}.
    \item Call \texttt{semWait} on \texttt{readSem}.
    \item Obtain a lock on \texttt{readCountMut}.
    \item IF \texttt{readCount} is equal to one, then:
        \begin{enumerate}
            \item Call \texttt{semWait} on \texttt{writeSem}.
        \end{enumerate}
    \item Release the lock on \texttt{readCountMut}.
    \item Call \texttt{semSignal} on \texttt{readSem}.
    \item Release the lock on \texttt{readSemMut}.
    \item Execute the writing operation.
    \item Obtain a lock on \texttt{readCountMut}.
    \item Decrement \texttt{readCount} by one.
    \item IF \texttt{readCount} is equal to zero, then:
        \begin{enumerate}
            \item Call \texttt{semSignal} on \texttt{writeSem}.
        \end{enumerate}
    \item Release the lock on \texttt{readCountMut}.
\end{enumerate}

\paragraph*{Procedure for writing}
\begin{enumerate}
    \item Obtain a lock on \texttt{writeCountMut}.
    \item Increment \texttt{writeCount} by one.
    \item IF \texttt{writeCount} is one, then:
        \begin{enumerate}
            \item Call \texttt{semWait} on \texttt{readSem}.
        \end{enumerate}
    \item Release the lock on \texttt{writeCountMut}.
    \item Call \texttt{semWait} on \texttt{writeSem}.
    \item Execute the write operation.
    \item Call \texttt{semSignal} on\texttt{writeSem}.
    \item Obtain a lock on \texttt{writeCountMut}.
    \item Decrement \texttt{writeCount} by one.
    \item IF \texttt{writeCount} is equal to zero, then:
        \begin{enumerate}
            \item Call \texttt{semSignal} on \texttt{readSem}.
        \end{enumerate}
    \item Release the lock on \texttt{writeCountMut}.
\end{enumerate}
