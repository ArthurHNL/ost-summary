\section{Deadlocks}
A \textbf{deadlock} is a set of processes and/or threads that are permanently
blocked because they are either waiting on communication from each other or competing
for a specific system resource. They are all waiting from an event that can only be
produced when one of these events is processed. An execution state of a set of
processes or threads where a deadlock is inevitable is called a \textbf{fatal region}.

\subsection{Conditions for a deadlock} \label{par:6-conditions}
A deadlock is only possible when the following conditions are all true:

\begin{enumerate}
    \item The requested resources are protected by a \textbf{mutual exclusion} mechanism.
    \item The process may \textbf{hold} the resources \textbf{and wait}
        for assignment of other resources.
    \item A resource cannot be forcibly removed from a process or thread, there is
        \textbf{no preemption}.
\end{enumerate}

Once all these conditions are met, the becomes inevitable when there is a \textbf{circular wait}
between processes and/or threads where every process holds at least one resource that is requested
by the next process/thread.

\subsection{Deadlock prevention}
\textbf{Deadlock prevention} mechanisms aim for removing at least one of the preconditions
stated in \autoref{par:6-conditions} \nameref{par:6-conditions} and/or removing the
possibility of a circular wait.

Generally speaking, mutual exclusion methods cannot be avoided.

The hold and wait condition can be prevented by only allowing processes to request all
required resources at once. Whilst this works great in theory, it does not work in practice
because processes not always know which resources they are going to require in the future.

The no preemption condition can also be prevented in multiple ways. An option would be to
force a process to release all its resources and re-request them when a resource request is
denied. This only works for resources whose state is easily stored and restored.

The circular wait condition can be prevented by an ordering of resource types and only
letting processes request resources in this order. This does however come with
enormous performance impact.

\subsection{Deadlock avoidance}
\textbf{Deadlock avoidance} mechanisms do not continually try to prevent the conditions stated
above. Instead, they dynamically decide if a resource request might lead to a deadlock and can
decide to deny a resource request in this case.

\subsubsection{Process initiation denial} \label{sec:6-process-denial}
This mechanism denies the initiation of a process when it could lead to a deadlock.
Given a system with $n$ processes and $m$ resource types, the total resource amount
of each resource type in the system is defined in \autoref{equ:6-resources},
the available amount of each resource type is defined in \autoref{equ:6-available}.
Following, the maximum amount of claims of each process for each resource type is stored in
\autoref{equ:6-claim} and the allocations to each process of each resource types is stored in
\autoref{equ:6-alloc}.

\begin{equation}
    \label{equ:6-resources}
    R=(R_{1}, R_{2}, \dots, R_{m})
\end{equation}
\begin{equation}
    \label{equ:6-available}
    V=(v_{1}, V_{2}, \dots, V_{m})
\end{equation}
\begin{equation}
    \label{equ:6-claim}
    C =
    \begin{bmatrix}
        C_{11}  &   C_{12}  & \dots     & C_{1m} \\
        C_{21}  &   C_{22}  & \dots     & C_{2m} \\
        \vdots  &   \vdots  & \vdots    & \vdots \\
        C_{n1}  &   C_{n2}  & \dots     & C_{nm}
    \end{bmatrix}
\end{equation}
\begin{equation}
    \label{equ:6-alloc}
    A =
    \begin{bmatrix}
        A_{11}  &   A_{12}  & \dots     & A_{1m} \\
        A_{21}  &   A_{22}  & \dots     & A_{2m} \\
        \vdots  &   \vdots  & \vdots    & \vdots \\
        A_{n1}  &   A_{n2}  & \dots     & A_{nm}
    \end{bmatrix}
\end{equation}

There are a few relationships that can be derived from these vectors and matrices.
First, \autoref{equ:6-available-or-alloced} states that all resources are either
allocated or available. Next, \autoref{equ:6-conservation-of-resources} states that
no process can claim more instances of a resource type then the total amount of
resources of that type in the system. Finally, \autoref{equ:6-no-alloc-above-quota}
states that no process may claim more resources then the amount of resources it is
allowed to in $C$.

\begin{equation}
    \label{equ:6-available-or-alloced}
    R_{j}=V_{j}+\sum^{n}_{i = i}A_{ij} \: \text{for all} \: j
\end{equation}

\begin{equation}
    \label{equ:6-conservation-of-resources}
    R_{ij} \leq R_{j} \: \text{for all} \: i,j
\end{equation}

\begin{equation}
    \label{equ:6-no-alloc-above-quota}
    A_{ij} \leq C_{ij}, \: \text{for all} \: i,j
\end{equation}

With these relationships in mind, we can define \autoref{equ:6-no-deadlock-alloc}. When this
holds true, a process can not request more resources that the total amount of resources in
the system and no deadlock can occur. A new process should only be allowed when this
equation holds true.

\begin{equation}
    \label{equ:6-no-deadlock-alloc}
    R_{j} \geq C_{n+1}j + \sum^{n}_{i=1}C_{ij}, \: \text{for all} \: j
\end{equation}

A problem with this mechanism is that it always assumes the worst possible outcome
and thus potentially causes huge performance issues.

\subsubsection{Resource allocation denial} \label{sec:6-resource-denial}
\textit{The matrices and vectors defined in \autoref{sec:6-process-denial} \nameref{sec:6-process-denial} are also used
in this part of the document.}

This strategy, also know as the \textbf{banker's algorithm} was first proposed by
Dijkstra in 1965. It defines a state for a system with a fix amount of $n$ processes,
$m$ resource types and $\sum^{m}_{i=1}R_{m}$ total resources where each process may have zero or
more resources allocated to it at a give time. This state is either \textbf{safe} when there
is at least one sequence of resource allocations that is guaranteed to not lead to a deadlock or \textbf{unsafe}
when this sequence does not exist. A system is in a safe state when \autoref{equ:6-safe-state} holds true, meaning
that the difference between the maximum requirement and current allocation be met with using the resources
of the system. When strictly enforcing a resource allocation denial policy, a resource request should be
denied if it would result in this equation no longer holding true.

\begin{equation}
    \label{equ:6-safe-state}
    C_{ij} - A_{ij} \leq V_{j} \: \text{for all} \: i,j
\end{equation}

The advantage of resource allocation denial is that it does not assume the worst case. It does, however,
have some preconditions for it to be used:

\begin{itemize}
    \item The maximum resource requirements for each resource type for each process
        must be registered and known in advance.
    \item Processes must not depend on each other.
    \item The maximum number of resources must be fixed.
    \item No process may exit when it holds at least one resource.
\end{itemize}

\pagebreak

\subsection{Deadlock detection}
Deadlock detection strategies are more conservative policies to deal with deadlocks, they
do not prevent deadlocks or resource allocations. Instead, they detect deadlocks and
attempt to solve them.

\subsubsection{Deadlock detection algorithm}
\textit{The matrices and vectors defined in \autoref{sec:6-process-denial} \nameref{sec:6-process-denial} are also used
in this part of the document.}

This algorithm detects deadlocks by accounting all possible sequences of tasks to be performed.

Given the already known matrices and vectors, together with matrix $Q$ that is defined in a way that
$Q_{ij}$ is equal to the amount of type $j$ requested by process $i$, this algorithm will mark each process
that is not part of a deadlocked set.

\begin{enumerate}
    \item Mark each process $i$ where $\sum^{m}_{j=0}A_{ij}=0$ since a process
        that has no resources allocated can not be part of a deadlock set.
    \item Initialize vector $W = V$ (the available vector).
    \item Search for index $i$ where process $i$ is not marked and
        $Q_{ik} \leq W_{k} \: \text{for} \: 1 \leq k \leq m$. When no such row is found,
        stop the algorithm.
    \item Mark process $i$ and add the corresponding row of the allocation matrix to $W$, that
        is: $W_{k} = W_{k} + A_{ik} \: \text{for} \: 1 \leq k \leq m$. Return to step 3.
\end{enumerate}

A deadlock exists iff there are unmarked processes at the end of the algorithm. The set of
unmarked processes then precisely corresponds to the set of deadlocked processes.

This algorithm works by finding a process whose resource requirements can be satisfied
with the available resources and then assume those resources are granted, the process
will run to it's completion and then release it resources. The algorithm then looks
for another process to satisfy.

Note that this algorithm only detects deadlocks that are currently in place, it
cannot predict if a deadlock is or is not possible.

\subsubsection{Deadlock recovery strategies}
Certain strategies are possible to recover from a deadlock:

\begin{enumerate}
    \item Abort all deadlocked processes. Most operating systems implement this strategy.
    \item Backup the deadlocked processes, restore a snapshot and hope that the non-deterministic
        behaviour of race conditions allows the restored state to continue normal execution
        without a new deadlock. This requires backup- and restore systems in place.
    \item Select the deadlocked process that has the lowest abortion cost and abort it.
        Run the deadlock detection algorithm again and keep doing this until the algorithm
        no longer reports a deadlock.
    \item Revoke resources until the deadlock no longer exists, with the same repetition and
        cost analysis as in (3).
\end{enumerate}

Criteria to select the lowest-cost victim for strategies (3) and (4) could be,
but are not limited to, one of the following criteria:

\begin{itemize}
    \item Least amount of processor time consumed so far.
    \item Least amount of output produced so far.
    \item Highest ETA.
    \item Least total resources allocated so far.
    \item Lowest priority.
\end{itemize}

\pagebreak

\subsection{Integrated deadlock strategy}
An OS should have an integrated deadlock strategy that uses multiple techniques
to prevent and/or detect and recover from deadlocks. An approach could be:

\begin{itemize}
    \item Group resources into resource classes.
    \item Use the linear ordering strategy for prevention of circular wait
        to prevent deadlocks between resource classes.
    \item Use relevant algorithms best suited for each resource class to
        prevent and/or detect and recover from deadlocks within each resource class.
\end{itemize}

\pagebreak

\subsection{The dining philosophers problem}
The dining philosophers problem is a problem regarding five philosophers that live in a house.
The philosophers want to have spaghetti for dinner. A table has been set up as shown in
figure \ref{fig:6-dining-philosophers}. When a philosophers eats, the philosopher picks up the
fork on the right and the fork on the left, eats its plate empty and then puts back both forks.
The task is to design an algorithm that lets all the philosophers eat concurrency whilst
ensuring mutual exclusivity over the forks and preventing both deadlocks and starvation.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.75\textwidth]{figures/include/6-deadlocks/philosophers}
    \caption{Dining philosophers problem}
    \label{fig:6-dining-philosophers}
\end{figure}

\subsubsection{Solution using semaphores}
Simply using a single semaphore for each fork will not work, this can result in a deadlock.
However, this deadlock can be prevented by adding an additional semaphore that only allows
four philosophers to access the dining room at the same time.

\subsubsection{Solution using a monitor}
Another possible solution involves a monitor that only allows one process to enter
the monitor at a given time. A monitor is used in conjunction with a monitor condition
variable for each fork and a boolean status indicator for each fork.
First, the philosophers claims a left fork. If it is not available, it will queue on the
monitor using the condition variable. Then this procedure is repeated for the right fork.
To release the forks, the monitor is signalled if required. The required availability
indicator boolean variables are updated accordingly.
