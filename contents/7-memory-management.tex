\section{Memory management}

\textbf{Memory management} is the task of dynamically subdividing 
user memory in order to accommodate multiple processes in a
multiprogramming system.

Some key terms are listed below.

\begin{itemize}
    \item \textbf{Memory frames}: fixed-length blocks of main memory.
    \item \textbf{Memory pages}: fixed-length blocks of data residing in
        secondary memory that can be (temporarily) copied into a frame of main memory.
    \item \textbf{Memory segments}: variable-length blocks of data residing in
        secondary memory that can be copied into an available region of main memory
        (\textbf{segmentation}) or divided into pages which can be individually copied
        into main memory (\textbf{segmentation combined with paging}).
    \item \textbf{Internal fragmentation}: the phenomenon that occurs when a process
        is placed in a container that is larger then the processes, the remainder of the
        size of the container is lost.
    \item \textbf{External fragmentation}: the phenomenon that occurs when main memory
        is divided into containers of different sizes and over time empty memory space
        between these containers is lost due to swapping and relocation.
    \item \textbf{Memory compaction}: the process of countering external fragmentation
        by relocating all containers onto a contiguous block of main memory.
    \item \textbf{Page table}: a table where the OS lists the frame locations for
        each page of each process.
    \item \textbf{Logical address}: a reference to a memory location independent
        of the current location of the memory.
    \item \textbf{Physical address}: a reference to an actual location in main memory.
    \item \textbf{Relative address}: an offset for a logical address.
\end{itemize}

\subsection{Requirements of a memory management system}
This part of the document describes the main requirements of a memory management
system of an OS of a multiprogramming system.

\subsubsection{Relocation}
Because the available main memory in the system is shared with other processes
and it must be able to swap processes from/onto secondary memory, it must be
able to \textbf{relocate} processes into different memory addresses and to translate
the memory references of the process to the physical memory addresses (because
the physical memory addresses may not be considered stable). 

\subsubsection{Protection}
Processes should be protected against unwanted interference from other processes.
Because memory can be relocated and dynamically allocated and deallocated at runtime,
this requires runtime check every time a process requests to read or change a
memory value. The requirement for relocation makes implementing this requirement harder
but certain mechanisms exist that support the implementation of both requirements.

This requirement must be satisfied on the hardware level because the OS cannot anticipate
all the memory request of a program and it would consume too much time to let the OS
check every memory request.

\subsubsection{Sharing}
Processes must be able to share a portion of main memory for example when the
same program is executed multiple times, the program code only has to be loaded
once into main memory.

\subsubsection{Logical organization}
The OS should support user programs that are logically divided into modules
that can be written and compiled independently and then be referenced
at runtime (dynamically linked libraries). The OS should offer some protection
in a way that modules can be read only or execute only and should offer some way
of sharing these modules between processes.

\subsubsection{Physical organization}
Because of the memory hierarchy and it would not be feasible to let processes
handle the transfer of data between main and secondary memory, the OS should
handle the transfer of data between main and secondary memory.

\subsection{Relocation}
Memory must be able to be relocated. To achieve this, the OS gives each word
a logical address which is passed to the processes and a physical
address which represents the current location of the word on physical memory.
Furthermore, a register is maintained that maps each logical address to the
valid physical address. Usually, the hardware offers some form of hardware
support to optimize this process.

\subsection{Memory management techniques}

\subsubsection{Partitioning}
Memory partitioning is the act of dividing main memory into partitions that
are to be used by processes. 

\paragraph*{Fixed partitioning} \mbox{} \newline
Fixed partitioning is a memory management technique where main memory is
divided into a number of static partitions that are created at system
generation time. A process is loaded into a partition of equal or greater size.

\subparagraph*{Placement algorithm} \mbox{} \newline
The placement algorithm of fixed partitioning is trivial; a process is placed
into the smallest memory partition available that is big enough to contain
the process.

\subparagraph*{Strengths and weaknesses} \mbox{} \newline
Strengths of the fixed partitioning memory management technique include
the fact that it is simple to implement and that the OS has little overhead.

Weaknesses of the fixed partitioning memory management technique include
inefficient use of main memory because of internal fragmentation and a
fixed maximum amount of active processes. Furthermore, processes that are
larger than the partition size, programs must ensure that they do not have
to be loaded entirely into memory at the same time.

\paragraph*{Dynamic partitioning} \mbox{} \newline
Dynamic partitioning is a memory management technique where main memory is
divided into partitions. These partitions are created dynamically with each
partition having the same size as the process that the partition contains.
The OS regularly compacts the memory to combat external fragmentation.

\subparagraph*{Placement algorithm} \mbox{} \newline
In order to reduce the time spent on memory compaction to a minimum, the
placement algorithm of dynamic partitioning is more complex. There are three
main strategies:

\begin{itemize}
    \item \textbf{First-fit}: Select the first available memory block.
        This strategy usually produces the best results.
    \item \textbf{Next-fit}: Select the first available memory block, starting
        from the position of the previous allocation. This strategy produces slightly
        worse results then first-fit because it fragments the largest block of free
        memory.
    \item \textbf{Best-fit}: Select the memory block that fits the best. This strategy
        causes the most fragmentation because it always leaves behind a small amount
        of free memory that is usually to small to allocate to another process. 
\end{itemize}

\subparagraph*{Strengths and weaknesses} \mbox{} \newline
Strengths of the dynamic partitioning memory management technique include
no internal fragmentation and efficient use of main memory.

Weaknesses of the dynamic partitioning memory management technique include
inefficient processor use because it has to compact memory in order to
counter external fragmentation.

\paragraph*{The buddy system} \mbox{} \newline
The buddy system is a compromise between dynamic and static partitioning.

In a buddy system that offers $2^K$ words, $L \leq K \leq U$, $2^L =$ the smallest
size of allocated memory block and $2^U = $ the largest size block that is allocated
and generally the size of main memory. In the beginning, the entire available space is treated
as a single block. The system maintains a list of holes (empty memory blocks) for each order
of memory block $i$ where the size of the block $= 2^i$ and $L \leq i \leq U$. In the event that
two sequential blocks (buddies) in the same order become deallocated, the two blocks are merged
into a single block and are moved to the relevant list.

\subparagraph*{Placement algorithm} \mbox{} \newline
The placement algorithm is shown in listing \ref{lst:7-buddy}

\begin{lstlisting}[
    caption = Placement algorithm of the buddy system that allocates $2^i$ words,
    label = lst:7-buddy]
get_hole :: int -> void*
get_hole i =
    if i == (U + 1) then
        <failure>
    else if i_list->is_empty then do {
        hole = get_hole i;
        buds :: tup<void*, void*> = split_hole_into_buddies(hole);
        push_back i_list first buds;
        push_back i_list second buds; 
    }
    pop i_list

\end{lstlisting}

\subsubsection{Paging}
\paragraph*{Simple paging} \mbox{} \newline
Simple paging is a memory management technique where main memory is divided
into a number of equally sized frames. Each process is divided into a number
of pages and can be loaded by loading all of the pages, that do not have to be
contiguous, into main memory. Each page is mapped into a memory frame and the
OS maintains a page table to map pages to frames.

\subparagraph*{Strengths and weaknesses} \mbox{} \newline
Strengths of the simple paging memory management technique include
no external fragmentation.

Weaknesses of the simple paging memory management technique include
a small amount of internal fragmentation.

\subsubsection{Segmentation}
\paragraph*{Simple segmentation} \mbox{} \newline
Simple segmentation is a memory management technique where each process is
divided into a number of segments. A process is loaded by loading all segments
into dynamically sized partitions, that do not have to be contiguous.

\subparagraph*{Strengths and weaknesses} \mbox{} \newline
Strengths of this technique include no internal fragmentation, improved
memory utilization and reduced overhead when compared to dynamic partitioning.

Weaknesses of this technique include external fragmentation.

\subsubsection{Virtual memory}
Virtual memory memory management techniques are described in the next
section but because they are memory management techniques, they are listed
in this section as well.

\paragraph*{Paged virtual memory} \mbox{} \newline
Paged virtual programming is like simple paging with the addition that not all
pages of a process have to be loaded at the same time. Pages that currently
not reside in main memory are loaded upon request.

\subparagraph*{Strengths and weaknesses} \mbox{} \newline
Strengths of this technique include no external fragmentation, a higher
degree of multiprogramming and a large virtual address space.

Weaknesses of this technique include that it introduces a complex overhead
to memory management.

\paragraph*{Segmented virtual memory} \mbox{} \newline
Segmented virtual memory is like segmentation with the addition that not all
segments of a process have to be loaded at the same time. Segments that currently
not reside in main memory are loaded upon request.

Strengths of this technique include no internal fragmentation, a higher degree
of multiprogramming, a large degree of virtual address space and support
for memory sharing and protection.

Weaknesses of this technique include that it introduces a complex overhead
to memory management.
