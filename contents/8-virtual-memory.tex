\section{Virtual memory}
Virtual memory is defined below, along with other key terms.

\begin{itemize}
    \item \textbf{Virtual memory}: a storage allocation scheme which
        allows secondary memory to be referenced as-if it were part of
        main memory.
    \item \textbf{Virtual address}: a reference to a location in a
        virtual memory space.
    \item \textbf{Virtual address space}: the virtual storage assigned to
        a process.
    \item \textbf{Address space}: the range of memory addresses available to
        a process.
    \item \textbf{Real address}: a reference to a location in main memory.
    \item \textbf{Real memory}: main memory.
    \item \textbf{Resident set}: the memory address ranges of a process that currently
        reside in real memory.
\end{itemize}

\subsection{Hardware-side and control structures of virtual memory}
The keys of virtual memory are two important characteristics
of segmentation and paging: processes have logical addresses instead of physical
addresses and the memory space of a process can be broken up into pieces. This leads
to the conclusion that if those two constraints hold, the entire address space of
a process does not have to be loaded into real memory in order to execute the
process. This has two primary benefits: more processes can be maintained in main
memory memory leading to more efficient use of the processor and processes that
are larger than the size of main memory can be instantiated.

In case a virtual address is referenced that is not loaded into main memory,
an interrupt is created that represents a memory access fault and the process
is suspended until the referenced data is loaded into main memory, allowing
the process to continue. Because of the principle of locality, memory faults
do not happen too often.

\subsubsection{Paged virtual memory}
The following memory formats are typically used in a paged virtual
memory system, where \texttt{P} is the present, bit \texttt{M} is the modified
bit and \texttt{B} represent other control bits:

\begin{itemize}
    \item \textbf{Virtual address} = \texttt{page number} + \texttt{offset}
    \item \textbf{Page table entry} = \texttt{P} + \texttt{M} + \texttt{B}
        + \texttt{frame number}
\end{itemize}

The present bit indicate if the page is currently present in real memory, the
modified bit indicate if it was modified since it was last loaded into real memory
and the frame number contains the frame number of the related real memory frame.
The frame number is usually the first real address of the frame.

Whenever a virtual address is referenced, the real address is then determined up by
looking up the frame number in the page table, looking up the first address of the
frame and adding the offset to this address.

This is, of course, an oversimplification. Usually, page tables are organised
in hierarchies where there is a root page table that contains references
to other page tables that may or may not be in real memory.

The drawback of this is that a page table is relatively big compared to the
real amount of addresses. To combat this, some operating system use an
\textbf{inverted page table}, where the page number part of a virtual address
is mapped to a hash value which then points to the inverted page table that in
turn contains the page table entries. This results in one entry for each memory
frame instead of for each virtual memory page.

The page size is hard to determine. A page size that is small results in
less internal fragmentation but requires more pages overall and thus
larger page tables.

\subsubsection{Segmented virtual memory}
The following memory formats are typically used in a segmented virtual
memory system:

\begin{itemize}
    \item \textbf{Virtual address} = \texttt{segment number} + \texttt{offset}
    \item \textbf{Segment table entry} = \texttt{P} + \texttt{M} + \texttt{B}
        + \texttt{length} + \texttt{segment base}
\end{itemize}

The segment number is the key to be used for the segment table. The segment table
contains the length of a segment together with the length of a segment.

Whenever a virtual address is referenced, the real address is then determined up by
looking up the start of the segment in the segment table and adding the
offset to this address.

\subsubsection{Segmented paged virtual memory}
The strengths of paging and segmentation can be combined. In such a system,
the following memory formats are typically used:

\begin{itemize}
    \item \textbf{Virtual address} = \texttt{segment number} + \texttt{page number}
        + \texttt{offset}
    \item \textbf{Segment table entry} = \texttt{B} + \texttt{length}
        + \texttt{segment base}
    \item \textbf{Page table entry} = \texttt{P} + \texttt{M} + \texttt{B}
        + \texttt{frame number}
\end{itemize}

In this case, the address space of a process is broken into segments.
These segments are then broken up into pages with the same size as a frame of
main memory. Each segment has a page table.
The segment number can then be used to look up a page table in which
the start of the page can be looked up. The starting location of the page
can then be combined with the offset to form the real address.

\subsubsection{Translation lookaside buffer (TLB)}
The TLB is a cache that maps virtual addresses to real addresses, in order to
optimize performance. A TLB is used by most virtual memory implementations.

\subsubsection{Protection and sharing}
Segmentation can facilitate implementation of protection and sharing mechanisms.
Because the address range of a segment is known, it is easy to check if
a process tries to access an address that it does not have access to.

Furthermore, some operating systems with a ring-like security system (such as UNIX),
may employ the following basic principles to protect system and memory integrity:

\begin{itemize}
    \item A process can access memory associated with a ring that has the
        same or less privilege levels.
    \item A process may call services residing on a ring that has the
        same or a higher privilege level.
\end{itemize}

\subsection{OS software \& virtual memory}
Designing the memory management mechanics of an operating system consists of
three fundamental questions:

\begin{enumerate}
    \item Should virtual memory be used?
    \item Should paging, segmentation, or both, be implemented?
    \item Which memory management algorithms shall be used?
\end{enumerate}

The answers for the first two questions mostly depend on what memory management
solutions the hardware platform that the OS will run on provide.
Most modern operating systems implement virtual memory using a combination of
paging and segmentation, with most of the design issues having something
to do with paging and not with segmentation. The operating system mostly
has to deal with answers to the third question. The answers of this question
can be categorized into policies, which are listed below.

\subsubsection{Fetch policy}
The fetch policy determines when a page shall be loaded into main memory.
There are two main policies: \textbf{demand paging} that loads a page
into memory when it is required and \textbf{pre-paging} which involves
loading multiple pages into main memory at the same time.

Usually this is combined, demand paging is inevitable in case of a page fault
and it is then more efficient to immediately load in more pages due to latency
and seek times of secondary memory.

\subsubsection{Placement policy}
The placement policy determines where in real memory a process piece is put
to reside. Because of efficient translation of virtual addresses, this is
usually not a concern and a first-fit algorithm is used. Only for
NUMA (non-uniform memory access) systems, systems where main memory
access times very per main memory module, should this be carefully considered
and research is still ongoing.

\subsubsection{Frame locking}
Certain page frames are locked in real memory. That is, their contents may
not be unloaded into virtual memory and then replaced. Usually, the frames
containing core parts of the kernel and other vital OS parts and I/O buffers are
locked.

\subsubsection{Replacement policy}
The replacement policy selects which page frame in real memory should be
replaced when a frame has to be loaded, from a predetermined set of frames
that are eligible to be replaced. This predetermined set of frames is
determined by the resident set management policy. The available policies
are listed below.

\paragraph*{Basic algorithms}
\subparagraph*{Optimal} \mbox{} \newline
The optimal policy aims to select the page for which it predict the
time to the next reference is the longest for replacement. This policy is known to be
the policy that results in the fewest amount of page faults. However, it is
not possible to perfectly implement this policy because it is not possible
to predict the future.

\subparagraph*{Least recently used (LRU)} \mbox{} \newline
The LRU policy selects the page that has not been referenced for the longest
amount of time for replacement. Because of the principle of locality, this should
be the page that is least likely to be referenced in the future. In practice,
this policy does nearly as well as the optimal policy.

\subparagraph*{First in first out (FIFO)} \mbox{} \newline
The FIFO policy selects the page that has been in memory for the longest amount
of time for replacement. This policy results in the most page faults but it does not
introduce as much overhead as the LRU policy.

\subparagraph*{Clock} \mbox{} \newline
The clock policy aims to achieve the page fault rate of the LRU policy but
with less memory management overhead.

Each memory has an extra associated bit named the use bit. The use bit is set to
\texttt{1} when a page is loaded into the frame and each time the frame is
referenced. When a frame is to be replaced, the OS enumerates over all the
frames where if the use bit is set to \texttt{1}, the use bit is set to \texttt{0} and
if the use bit is \texttt{0}, the frame is selected for replacement. The OS keeps
enumerating over all the eligible frames until a frame is found to be replaced.

\paragraph*{Page buffering} \mbox{} \newline
Page buffering is a strategy that involves less overhead then LRU and clock but it has
a better performance. To improve performance, replaced pages are not lost but placed
at the tail of either a free page list if the page has not been modified or a modified
page list if it has. The page is not physically moved in main memory, only the
page table entry is.
When a page is to be read in, a page is taken from the free page list and replaced
with the page to be read in. A similar process occurs with the modified
pages.
This allows modified pages to be written in clusters, thus saving time.

\subsubsection{Resident set management policy}
The resident set management policy deals with the following concepts:

\begin{enumerate}
    \item The number of page frames allocated to each active process.
    \item If the set of page frames considered to be replaced should either only
        only consist of the page frames of the process that caused the page fault,
        or should consist of all the page frames in real memory.
\end{enumerate}

The first concept is named the \textbf{resident set size}. The second concept
is referred to as the \textbf{replacement scope}.

The resident set size can have either a \textbf{variable allocation policy},
which allows the number of page frames associated with a process to change
over the lifetime of a process, or a \textbf{fixed allocation policy}, which
gives each process a fixed amount of page frames that is determined when
the process is spawned. Whilst the variable allocation policy sounds more attractive,
it does introduce more complexity and overhead as the OS has to assess the behaviour
of active processes.

The replacement scope can be set to either a \textbf{local replacement policy}
which allows only page frames of the process that caused the page fault to occur
to be replaced, or a \textbf{global replacement policy} which allows every page
frame in real memory to be replaced.

There is a correlation between the two concepts. All possible combinations of
resident set size policies and replacement scope policies are listed below.
The combination of a fixed allocation resident set size with a global replacement
scope is not possible.

\paragraph*{Fixed allocation \& local replacement scope} \mbox{} \newline
This policy has the major disadvantage that if allocations are too small,
there will be a high page fault rate and if allocations are too large,
the processor will be either wasting time being idle or wasting time
swapping processes.

\paragraph*{Variable allocation \& global replacement scope} \mbox{} \newline
This policy is the easiest policy to implement with a single challenge:
the replacement choice. Because the frame chosen to replace may be from
a different process that is also executing, this can cause performance issues.
To counter this, page buffering techniques can be implemented.

\paragraph*{Variable allocation \& local replacement scope} \mbox{} \newline
This strategy attempts to overcome the problems of the variable \& global strategy
but it is significantly harder to implement. The implementation of this policy
usually looks as follows:

\begin{enumerate}
    \item Allocate a set amount of frames to a process when it is spawned.
        Fill up the frames using either pre-paging or demand paging.
    \item When a page fault occurs, select a frame to replace from the
        frames of the process.
    \item Periodically evaluate if the amount of frames allocated to the
        process should be increased or decreased to improve overall performance.
\end{enumerate}

The last step is the step that introduces the most complexity. A strategy
regarding this step is the \textbf{working set strategy}.

\tipbox{
    In set theory, the symbol "$\supseteq$" in "$A \supseteq B$" means
    "is a superset of". Thus, in this example, A is a superset of B and B
    is a subset of A. Note that it does not
    have to be a proper superset, the two sets may be equal.
}

\tipbox{
    In set theory, the notation "$|A|$" means "the number of elements
    in set A".
}

This strategy first designates the set of pages a process has referenced
over the last $\Delta$ virtual time units as $W(t, \Delta)$, where
$\Delta$ is the virtual time window over which the process is observed and
$t$ is the current virtual time. Virtual time is measured in memory
references and is thus an integer that only increases over real time.

\begin{equation}
    \label{equ:8-window-size-set-rel}
    W(t, \Delta + 1) \supseteq W(t, \Delta)
\end{equation}

\begin{equation}
    \label{equ:8-set-size-single-page}
    n_{p} = 1 \Rightarrow |W(t, \Delta)| = 1
\end{equation}

\begin{equation}
    \label{equ:8-max-set-size}
    1 \leq |W(t, \Delta)| \leq \min{(\Delta, N)}
\end{equation}

\autoref{equ:8-window-size-set-rel} states that the larger the time window size,
the larger the referenced page set. \autoref{equ:8-set-size-single-page} states
that if the number of pages used by a process $n_{p}$ over a time window
is equal to $1$, the set should only contain one element. Finally, the set may
be as large as the number of pages $N$ allocated to the process, as long as the
window size allows it, as is stated by \autoref{equ:8-max-set-size}.

This concept can be used to setup the following policy for managing the working
set of each process:

\begin{enumerate}
    \item Monitor the working set of each process.
    \item Periodically remove the pages that are not in the working set
        from the resident set (LRU policy).
    \item Only let a process execute if it's working set is in main
        memory.
\end{enumerate}

Of course, this strategy also has a number of problems:

\begin{enumerate}
    \item The size and membership of the process will change over time because
        the past does not predict the future.
    \item A true measurement for the working set introduces too much overhead.
    \item The optimal value of $\Delta$ is variable and unknown.
\end{enumerate}

Instead of monitoring the working set size directly, similar results can be achieved
by monitoring the page fault rate. If it is below a minimum threshold, the system must
deallocate pages from the resident set of the process so the system can benefit as a whole. If the
page fault rate is above a maximum threshold, the system must allocate a page to the resident
set of the process to decrease overall page faults on the system.

An algorithm that implements this policy is the \textbf{page fault frequency
(PFF) algorithm}. This algorithm maintains a bit for each page in memory that is
set to \texttt{1} when the page is accessed and defines a threshold $F$.
When a page fault occurs, the system determines the virtual time passed $\Delta$ for
that specific process since the last page fault. When $\Delta < F$, a page is
allocated to the resident set. Otherwise, discard all pages where the bit is
set to \texttt{0} from the resident set and deallocate the pages. In all cases,
the bit is set to \texttt{0} for all pages of the resident set.

This algorithm does have a flaw, when the process switches locality, no page
is deallocated before the timer expires. Another policy that does deal with this
phenomenon is referred to as the \textbf{variable-interval sampled working set
(VSWS) policy} and can be seen as an modified version of the PFF algorithm because
it still uses the same bit for each page.

This policy is driven by the following parameters:

\begin{itemize}
    \item The minimum duration of the sampling interval $M$.
    \item The maximum duration of the sampling interval $L$.
    \item The number of page faults allowed to occur between
        sampling instances $Q$.
\end{itemize}

The policy operates as follows:

\begin{enumerate}
    \item When the virtual time since the last sampling instance reaches $L$, suspend
        the process and scan the bits.
    \item When, prior to an elapsed virtual time of $L$ $Q$ page faults occur:
        \begin{enumerate}
            \item When the virtual time since the last sampling instance is less then $M$,
                then wait until the elapsed virtual time reaches $M$ and continue to step b.
                Otherwise, immediately continue to step b.
            \item Suspend the process and scan the bits.
        \end{enumerate}
\end{enumerate}

Case studies have shown that VSWS is as simple to implement as, and more effective
than, PFF.

\subsubsection{Cleaning policy}
The cleaning policy is the opposite of the fetch policy and is concerned with
determining when a modified page should be written to secondary memory.

A \textbf{demand cleaning} policy will write out a page when it is selected
for replacement and not earlier then when it is selected for replacement. This
minimizes the amount of page writes on the cost that it may require two page
accesses when replacing a page.

A \textbf{pre-cleaning} policy will write out a page before it is replaced so
that pages can be written out in batches but does increase the total amount
of page writes.

Page buffering is usually a better solution.

\subsubsection{Load control policy}
The load control policy determines how many processes may be resident
in main memory at a given time. It mainly deals with two aspects:
the multiprogramming level and process suspensions. Both are described
further below.

Load control is critical because if too few processes are resident
at a given time, the processor has nothing to do and if too many processes
are resident at the same time, the average resident size of a process will no
longer be adequate.

\paragraph*{Multiprogramming level} \mbox{} \newline
As the multiprogramming level increases, the processor usage increases
until a cut-off point is reached where the average resident set size
is no longer adequate. To counter this, the number of active processes
must be limited. Some studies suggest that utilizing 50\% of paging capacity
correlates to maximum processor efficiency.

\paragraph*{Process suspension} \mbox{} \newline
If the degree of multiprogramming is to be reduced, some processes
must be suspended. Certain strategies for selecting a process to be
suspended exist, these strategies are listed below.

\begin{enumerate}
    \item \textbf{Lowest-priority}: A scheduling decision unrelated to
        performance could be to suspend the process with the lowest priority.
    \item \textbf{Faulting process}: Faulting processes have a high probability
        of not having their working set present and is about to be blocked anyway.
    \item \textbf{Process with the smallest resident set}: Requires the least
        amount of effort but unfairly penalizes processes with a high locality.
    \item \textbf{Process with the largest remaining execution window}:
        Can be helpful in enforcing a shortest-processing-time-first scheduling
        discipline.
\end{enumerate}
