\section{Uniprocessor Scheduling}
Scheduling is the process of determining when which process is allowed
to execute.

\subsection{Types of scheduling}
There are multiple types of scheduling that all involve a different decision. The
four types of scheduling are listed below. The frequency that a scheduler runs
is the highest for the short-term scheduler, lower for the medium-term scheduler
and the lowest for the long-term scheduler.

\begin{itemize}
    \item \textbf{Long-term scheduling}: scheduling that involves the decision of
        adding a process to the pool of processes to be executed, or the action
        of admitting a process. In a certain sense, it controls the degree of
        multiprogramming. Because of this, the long-term scheduling policy is
        mainly driven by the desired degree of multiprogramming. Once admitted,
        the process becomes enqueued for either short-term scheduling when
        admitted to the ready state or enqueued for medium-term scheduling
        when admitted to a suspended state.
    \item \textbf{Medium-term scheduling}: scheduling that involves the decisions
        of activating and suspending a process and is part of the swapping function.
        It is mainly driven by the desired degree of multiprogramming and virtual
        memory management.
    \item \textbf{Short-term scheduling}: scheduling that involves the decisions
        of dispatching a process and giving a process a time-out. This scheduler
        runs most often and must make a fine-grained decision of which process
        to execute next. The scheduler is invoked every time an event happens that
        may lead to the current process being blocked or that may provide an opportunity
        to let another process execute.
\end{itemize}

\subsection{Scheduling algorithms}

\subsubsection{Criteria for short-term scheduling algorithms}
A list of key criteria that can be used to compare scheduling algorithms
is shown below. Note that these are interdependent and it is not possible
to optimize all of them.

\begin{enumerate}
    \item \textbf{User oriented \& performance related criteria}:
        \begin{enumerate}
            \item \textbf{Turnaround time}: the interval of time between the
                submission of a process and the completion of a process and thus includes
                both execution and idle time. An appropriate measure for a batch job.
                Lower is better.
            \item \textbf{Response time}: the time from the submission of a request until
                the response begins to be received. Only a useful metric for interactive
                processes. Lower is better. The most critical requirement for most
                interactive operating systems.
            \item \textbf{Deadlines}: the percentage of deadlines that are met.
                Higher is better.
    \end{enumerate}
    \item \textbf{User oriented \& non-performance related criteria}:
        \begin{enumerate}
            \item \textbf{Predictability}: the variance in turnaround time and
                response time for a given job. Lower is better.
        \end{enumerate}
    \item \textbf{System oriented \& performance related criteria}:
        \begin{enumerate}
            \item \textbf{Throughput}: the number of processes completed per unit
                of time. This metric depends on the average lifespan of a process
                and could also be defined as the amount of work performed per unit
                of time. Higher is better.
            \item \textbf{Processor utilization}: the percentage of time that the
                processor is busy. Higher is better.
        \end{enumerate}
    \item \textbf{System oriented \& non-performance related criteria}:
        \begin{enumerate}
            \item \textbf{Fairness}: an indicator for how processes are treated
                the same and no process suffers from starvation. Higher is better.
            \item \textbf{Enforcing priorities}: an indicator for how good the scheduler
                is at favouring higher-priority processes. Higher is better.
            \item \textbf{Balancing resources}: an indicator for how the policy favours
                processes that under-utilize system resources that are under stress.
                Higher is better, also applies to medium- and long-term scheduling.
        \end{enumerate}
\end{enumerate}

\subsection{Use of priorities}
Most OSes assign a priority to each process and the scheduler then has a queue
for each priority. When the scheduler needs to pick a process, it first
picks one from the high priority queue and if that queue is empty, it looks
into a lower priority queue. This can however lead to starvation. To counter this,
sometimes the priority of a process is altered based on its age or execution history.

\subsection{Alternative scheduling policies}
A summary of popular scheduling policies is shown in \autoref{tbl:9-scheduling}.
The \textbf{selection function} selects the next process to be executed from the
processes that are ready. The other three significant variables are as follows:

\begin{itemize}
    \item $w =$ total time spent in the system so far.
    \item $e =$ total time spent in execution so far.
    \item $s =$ total service time required by the process, including $e$. This value is
        either estimated or user-supplied.
\end{itemize}

\begin{table}[h]
    \centering
    \begin{tabular}{C {5em}| C{5em} | C{5em} | C{5em} | C{5em} | C{5em} | C{5em}}
                                        &   \textbf{FCFS}               &   \textbf{Round Robin}    &   \textbf{SPN}                &   \textbf{SRT}                &   \textbf{HRRN}               &   \textbf{Feedback}               \\ \hline
        \textbf{Selection function}     &   $\max{[W]}$                 &   constant                &   $\min{[s]}$                 &   $\min{[s - e]}$             &   $\max{(\frac{w + s}{s})}$   &   \textit{see description}        \\ \hline
        \textbf{Decision mode}          &   Not preemptive              &   Preemptive at time quantum  &   Not preemptive          &   Preemptive at arrival       &   Not preemptive            & Preemptive at time quantum          \\ \hline
        \textbf{Throughput}             &   Not emphasized          &   Can be low if quantum is too small & High                   &   High                        &   High                        & Not emphasized                    \\ \hline
        \textbf{Response time}          &   Can be high, especially when there is a large variance in process execution times   &   Provides good response time for short processes &   Provides good response time for short processes  &   Provides good response time &   Provides good response time &   Not emphasized \\ \hline
        \textbf{Overhead}               &   Minimum                     &   Minimum                 &   Can be high                 &   Can be high                 &   Can be high                 & Can be high                       \\ \hline
        \textbf{Effect on processes}    &   Penalizes short \& IO-bound processes   &   Fair treatment  &   Penalizes long processes    &   Penalizes long processes    &   Good balance    &   May favour IO-bound processes           \\ \hline
        \textbf{Starvation}             &   no                          &   No                      &   Possible                    &   possible                    &   No                  &   Possible
    \end{tabular}
    \caption{Comparison of scheduling policies}
    \label{tbl:9-scheduling}
\end{table}

The \textbf{decision mode} specifies the instants in time at which the selection
function is executed. A mode can fall into two general categories: \textbf{
preemptive} where a process may be moved to the ready state by the OS and
\textbf{not preemptive} where a process in the running state either
becomes blocked or terminates (it does not time-out).

Generally, preemptive policies introduce more overhead than policies that are
not preemptive but may increase overall service levels by preventing the
processor from begin monopolized by a single process.

The individual scheduling policies will now be described in detail.

\subsubsection{First Come, First Served (FCFS)}
FCFS (also known as First In, First Out (FIFO)) is the simplest scheduling policy.
Processes are placed in the ready queue when they become ready and the execution
is based on the ordering of the ready queue.

FCFS performs best with long-running processes. It tends to favour processor-bound
processes over IO-bound processes and can result in insufficient use of IO devices.

FCFS is not an attractive alternative on its own but it is often combined
with priority queues.

\subsubsection{Round Robin}
Round Robin is a clock-based preempting scheduler. Each process is given
a time slice before being preempted and another process is dispatch. This phenomenon
is also known as \textbf{time slicing}.

The length of the slice, also known as \textbf{time quantum}, is the primary
design issue. If it is too long, the turnaround time will increase. If it is
too short, overhead will take more time then actual execution. A general rule is
that the time quantum should be slightly greater than the time required for a typical
interaction or process function.

Round robin excels in general-purpose time-sharing systems and transaction-based systems.
A drawback is the difference in treatment of IO-bound and processor-bound processes.
Because IO-bound processes spend most of their time in the queue, the usage efficiency
of IO-devices decrease and the variance of response time increases. This can be
solved by using a separate queue for processes that were previously blocked on IO,
this queue gets priority over processes from the ready queue and are then allowed
to execute for no longer then the time quantum minus the time already spent
executing before they got blocked.

\subsubsection{Shortest Process Next (SPN)}
The SPN policy is a policy that is not preemptive and opts for the process
that has the lowest execution time left.

A pitfall of this policy is that the scheduler must know (at least the estimate of)
the required processing time for each process. In production environments, statistics
may be used to determine the average running time for specific times of jobs. In
interactive environments, the OS can keep averages of the "bursts" of processes

\begin{equation}
    \label{equ:9-spn-exp-avg-time}
    S_{n + 1} = \alpha T_n + (1 - \alpha)S_n
\end{equation}

\autoref{equ:9-spn-exp-avg-time} shows a calculation for the predicted
execution time, where:

\begin{itemize}
    \item $T_i =$ the processor execution time for the $i$th instance of this
        process (total time for a batch job or the burst time for an interactive job).
    \item $S_i =$ predicted value for the $i$th instance.
    \item $S_1 =$ predicted value for the first instance; this value is not calculated.
    \item $\alpha =$ a constant weighing factor that determines the relative weight
        given to recent observations over older observations. The constraint
        $0 < \alpha < 1$ must always hold. This technique is known as \textbf{
        exponential averaging}.
\end{itemize}

A risk of using SPN is starvation: longer processes can be withheld from
being executed as long as there is plenty of supply of shorter processes.

\subsubsection{Shortest Remaining Time (SRT)}
SRT is a preemptive version of SPN that always chooses the process that has the
shortest expected remaining process time. The scheduler is triggered when a
process enters the ready queue and will then preempt the current process when
the process that entered has a shorter time remaining than the current process.

As with SPN, there must be a way to calculate a (prediction of) the remaining time
and there is a risk of starvation of longer processes.

Unlike FCFS, SRT does not bias long running processes and unlike round robin, SRT
uses no additional interrupts so the total amount of overhead is reduced.

\subsubsection{Highest Response Ratio Next (HRRN)}
HRRN is a scheduling policy that aims to minimize the response ratio of each process.

\begin{equation}
    \label{equ:9-hrrn-ratio}
    R = \frac{w + s}{s}
\end{equation}

\autoref{equ:9-hrrn-ratio} states the response ratio of a process, with:

\begin{itemize}
    \item $R =$ the response ratio.
    \item $w =$ the time spent waiting for the processor.
    \item $s =$ the expected service time.
\end{itemize}

The minimum value of $R$ is $1.0$, this value occurs when a process enters the system.
If the process is dispatched immediately, the value will be equal to the turnaround time.

The scheduling rule is to choose the process with the lowest $R$ value. This is an attractive
approach because it accounts for the age of the process. Shorter jobs are favoured first but
longer jobs will eventually get chosen as well. As with SRT and SPN, there must be a way
to determine the (estimated) expected service time.

\subsubsection{Feedback (FB)}
Feedback is a time-quantum based preemptive scheduler that can be used when there is no reliable
way of estimating the time required for a process. It penalizes jobs that have been running longer.

The scheduler uses a dynamic priority system. The first time that a process is to be scheduled,
it is placed in queue $RQ_0$. The second time, it is placed in $RQ_1$. The $n$th time, it is placed
in $RQ_{n -1}$. A short process will complete fairly quickly whilst long-running jobs will quickly
be moved down the hierarchy.

Starvation can occur when this policy is implemented. To prevent this, processes can be
shifted into a higher queue when they appear to never get executed.

\subsection{Performance comparison of scheduling algorithms}
Whilst is is impossible to make definitive comparisons between scheduling algorithms
because relative performance is based on too much variables, including performance of and
demand for IO, some general conclusions can be drawn.

\subsubsection{Queuing analysis}
Basic queuing formulas can be combined with the common assumptions of Poisson
(essentially random) arrivals and exponential service types to analyze
queuing performance. This summary does not contain these formulas.

\subsubsection{Simulation modelling}
Discrete-event simulation shows that the turnaround time of FCFS is very
unfavourable, $\frac{1}{3}$ of the processes have a normalized turnaround
greater then 10 times the service time, with the absolute waiting time being
uniform.

Round rubin yields a normalized turnaround time of around 5 for all processes, with all
processes being treated fairly. SPN performs better than round robin except for short processes.
SRT performs better than SPN except for the 7\% longest processes.

FB performs well for relatively short processes. 

\subsection{Fair-share scheduling}
Fair-share scheduling is an approach to scheduling where scheduling decisions are based
on the set of processes that the user is currently using most and with system resources
being distributed fairly among multiple users of the system.

Scheduling is done on priority basis, taking into account the underlying priority
of the process, its recent processor usage, the recent processor usage of the group to which
the process belongs.

\begin{equation}
    \label{equ:9-cpu-util-proc}
    CPU_j(i) = \frac{CPU_j(i - 1)}{2}
\end{equation}

\begin{equation}
    \label{equ:9-cpu-util-group}
    GCPU_k(i) = \frac{GCPU_k(i - 1)}{2}
\end{equation}

\begin{equation}
    \label{equ:9-proc-prio}
    P_j(i) = Base_j + \frac{CPU_j(i)}{2} + \frac{GCPU_k(i)}{4W_k}
\end{equation}

The equations \ref{equ:9-cpu-util-proc}, \ref{equ:9-cpu-util-group} and
\ref{equ:9-proc-prio} apply for process $j$ in group $k$ when fair-share scheduling
is implemented, where:

\begin{itemize}
    \item $CPU_j(i) =$ measure of processor utilization by process $j$ through interval $i$.
    \item $GCPU_k(i) =$ measure of processor utilization by group $k$ through interval $i$.
    \item $P_j(i) =$ the priority of process $j$ at the beginning of interval $i$. A lower value
        indicates a higher priority.
    \item $Base_j =$ the base priority of process $j$. A lower value indicates a higher
        priority.
    \item $W_k =$ a weighting factor assigned to group $k$, complicit to the constraints
        that $0 < W_k \leq 1$ and $\sum_kW_k = 1$.
\end{itemize}
